import { StyleSheet, Platform } from 'react-native';
import { fonts } from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 40 : 20,
    paddingHorizontal: '5%'
  },
  headerText: {
    flex: 1,
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: 'white',
  },
  SettingsHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  emergency: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingVertical: 3,
  },
  emergencyText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: 'black',
  },
  labelTest: {
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    color: '#2A78C6',
  },
  containerLabelTest: {
    backgroundColor: 'white',
    width: 140,
    height: 35,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
