import {StyleSheet, Platform} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 0,

    elevation: 10,
    zIndex: 1
  },
  search: {
    marginVertical: 15, 
    paddingHorizontal: '5%'
  },
  inputStyle: {
    fontFamily: fonts.robotoMedium,
    fontSize: 12,
    color: colors.REGULAR_COLOR,
    marginLeft: 10,
  },
  inputContainerStyle: {
    backgroundColor: '#E8E8E8',
    borderRadius: 10,
    borderBottomWidth: 0
  },
  containerStyle: {
    paddingHorizontal: 0,
  },
  headerText: {
    flex: 1,
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: 'white',
  },
  defaultHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  emergency: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingVertical: 3,
  },
  emergencyText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: 'black',
  },
  tabContainer: {
    flex: 1,
    alignItems: 'center',
  },
  activeTabText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 18,
    color: 'white',
  },
  inactiveTabText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
    color: 'white',
  },
  divider: {
    width: 70,
    height: 3,
    borderRadius: 2,
    backgroundColor: 'white',
  },
  tabNavigationContainer: {
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
