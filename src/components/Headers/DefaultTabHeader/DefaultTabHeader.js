import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native';
import { Icon, Divider, Input } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
import { colors } from '../../../constants';

class DefaultTabHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: 'medication'
    };
  }

  navigationHandler = routeName => {
    this.props.navigation.navigate(routeName);
    routeName === 'Medication' ? this.setState({screen: 'medication'}) : this.setState({screen: 'symptom'}) 
  };

  onPressBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { navigation, getLabelText } = this.props;
    const routes = navigation.state.routes;
    return (
      <LinearGradient
        colors={[colors.HOME_MAIN_GRADIENT_START, colors.HOME_MAIN_GRADIENT_END]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ height: 166 }}>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <View style={styles.defaultHeader}>
              <View style={styles.emergency}>
                <Text style={styles.emergencyText}>Emergency</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Icon
                  name="battery-50"
                  type="material-community"
                  color="white"
                  size={33}
                />
                <Icon
                  name="ios-settings"
                  type="ionicon"
                  color="white"
                  underlayColor="transparent"
                  size={33}
                  onPress={() => navigation.navigate('Settings')}
                />
              </View>
            </View>
            <View style={styles.tabNavigationContainer}>
              <Icon
                name="arrow-left"
                type="material-community"
                color="white"
                underlayColor="transparent"
                onPress={this.onPressBack}
              />
              {routes.map((route, index) => {
                return navigation.state.index === index ? (
                  <TouchableOpacity
                    onPress={() => this.navigationHandler(route.routeName)}
                    style={styles.tabContainer}>
                    <Text style={styles.activeTabText}>
                      {getLabelText({ route })}
                    </Text>
                    <Divider style={styles.divider} />
                  </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                      onPress={() => this.navigationHandler(route.routeName)}
                      style={styles.tabContainer}>
                      <Text style={styles.inactiveTabText}>
                        {getLabelText({ route })}
                      </Text>
                    </TouchableOpacity>
                  );
              })}
            </View>
            <View style={styles.search}>
              <Input
                placeholder={"Search for a " + this.state.screen}
                leftIcon={{
                  name: 'magnify',
                  type: 'material-community',
                  color: colors.REGULAR_COLOR,
                }}
                inputStyle={styles.inputStyle}
                inputContainerStyle={styles.inputContainerStyle}
                containerStyle={styles.containerStyle}
              />
            </View>
          </View>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

export default DefaultTabHeader;
