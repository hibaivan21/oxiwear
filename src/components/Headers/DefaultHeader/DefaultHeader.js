import React from 'react';
import { View, Text, StatusBar, SafeAreaView, Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { colors } from '../../../constants';

import styles from './styles';

const DefaultHeader = ({ ...props }) => {
  const { options } = props.scene.descriptor;
  const title =
    options.headerTitle !== undefined
      ? options.headerTitle
      : options.title !== undefined
        ? options.title
        : props.scene.route.routeName;
  return (
    <LinearGradient
      colors={[colors.HOME_MAIN_GRADIENT_START, colors.HOME_MAIN_GRADIENT_END]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={[{ height: 170 }, options.headerStyle]}>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={styles.defaultHeader}>
            <View style={styles.emergency}>
              <Text style={styles.emergencyText}>Emergency</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Icon
                name="battery-50"
                type="material-community"
                color="white"
                size={33}
                onPress={() => props.navigation.navigate('PowerLevel')}
              />
              <Icon
                name="ios-settings"
                type="ionicon"
                color="white"
                underlayColor="transparent"
                size={33}
                onPress={() => props.navigation.navigate('Settings')}
              />
            </View>
          </View>
          <View
            style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
            {props.previous && title ==! 'PowerLevel' ? (
              <Icon
                //containerStyle={{flex: 1}}
                underlayColor="transparent"
                color="white"
                name="arrow-left"
                type="material-community"
                onPress={() => props.navigation.goBack()}
              />
            ) : null}
            <Text
              style={[
                styles.headerText,
                props.previous ? { textAlign: 'center' } : null,
              ]}>
              {title}
            </Text>
            {props.showTest ? (
              <View style={styles.containerLabelTest}>
                <Text style={styles.labelTest}>Test History</Text>
              </View>
            ) : null}
          </View>
        </View>
      </SafeAreaView>
    </LinearGradient>
  );
};

export default DefaultHeader;
