import {StyleSheet, Dimensions} from 'react-native';
import {fonts} from '../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: Dimensions.get('window').width,
    alignItems: 'center',
    justifyContent: 'center',
  },

  headerText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: 'white',
    marginVertical: 20,
    textAlign: 'center',
  },
  headerText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: 'white',
    marginVertical: 20,
    textAlign: 'center',
  },
  footerText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: 'white',
    marginVertical: 20,
    textAlign: 'center',
  },
  logoText: {
    marginTop: 30,
  },
  contentBlock: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  gradientContainer: {
    flex: 1,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    zIndex: 5,
    elevation: 10
  },
});
