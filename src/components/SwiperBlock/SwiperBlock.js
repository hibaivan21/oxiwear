import React from 'react';
import {View, Text} from 'react-native';

import styles from './styles';

const SwiperBlock = ({item}) => (
  <View style={styles.container}>
    <View style={styles.contentBlock}>
      <Text style={styles.headerText}>{item.headerText}</Text>
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        {item.image}
      </View>
      <Text style={styles.footerText}>{item.footerText}</Text>
    </View>
  </View>
);

export default SwiperBlock;
