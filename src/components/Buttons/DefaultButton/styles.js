import {StyleSheet} from 'react-native';

import {colors, fonts} from '../../../constants';

export default StyleSheet.create({
  buttonTitle: {
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.BUTTON_TITLE,
  },
  buttonStyle: {
    backgroundColor: colors.UNACTIVE_PAGINATION,
    borderRadius: 10,
    height: 40,
    marginHorizontal: '23%',
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 0,

    elevation: 3,
  },
  buttonContainer: {
    marginVertical: 5,
  },
});
