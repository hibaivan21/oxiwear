import React from 'react';
import {Button} from 'react-native-elements';

import styles from './styles';

const DefaultButton = ({...props}) => (
  <Button
    {...props}
    titleStyle={styles.buttonTitle}
    buttonStyle={styles.buttonStyle}
    containerStyle={styles.buttonContainer}
  />
);

export default DefaultButton;
