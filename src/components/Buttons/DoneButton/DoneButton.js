import React from 'react';
import {Button} from 'react-native-elements';

import styles from './styles';

const DoneButton = ({...props}) => (
  <Button
    {...props}
    titleStyle={[styles.buttonTitle, props.buttonTitle]}
    buttonStyle={[styles.buttonStyle, props.buttonStyle]}
    containerStyle={[styles.buttonContainer, props.containerStyle]}
  />
);

export default DoneButton;
