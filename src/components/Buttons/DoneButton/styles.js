import {StyleSheet} from 'react-native';

import {colors, fonts} from '../../../constants';

export default StyleSheet.create({
  buttonTitle: {
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  buttonStyle: {
    backgroundColor: colors.DONE_BUTTON_BACKGROUND,
    borderRadius: 10,
    height: 40,
    marginHorizontal: '37%',
  },
  buttonContainer: {
    marginVertical: 5,
  },
});
