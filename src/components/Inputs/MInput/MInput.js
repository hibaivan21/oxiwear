import React from 'react';
import {Input} from 'react-native-elements';

import styles from './styles';
import {colors} from '../../../constants';

const MInput = ({...props}) => (
  <Input
    {...props}
    placeholderTextColor={colors.PLACEHOLDER_TEXT}
    labelStyle={styles.labelStyle}
    inputStyle={styles.inputStyle}
    inputContainerStyle={styles.inputContainerStyle}
    containerStyle={styles.containerStyle}
  />
);

export default MInput;
