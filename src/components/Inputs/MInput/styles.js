import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  labelStyle: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: fonts.robotoRegular,
    fontWeight: '300',
    color: colors.HEADER_COLOR,
  },
  inputStyle: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    textAlign: 'center',
  },
  inputContainerStyle: {
    height: 40,
    width: 100,
    borderRadius: 10,
    paddingHorizontal: 15,
    backgroundColor: 'white',
    borderWidth: 0.5,
    borderColor: colors.BORDER,
  },
  containerStyle: {
    paddingHorizontal: 0,
    width: 100,
  },
});
