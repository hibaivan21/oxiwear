import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  inputContainerStyle: {
    height: 50,
    borderRadius: 20,
    paddingHorizontal: 15,
    backgroundColor: 'white',
    marginHorizontal: '10%',
  },
  inputContainer: {
    marginVertical: 5,
  },
});
