import React from 'react';

import styles from './styles';
import {colors} from '../../../constants';
import { TextInput } from 'react-native-gesture-handler';

const DefaultInput = React.forwardRef((props, ref) => (
  <TextInput
      {...props}
      ref={ref}
      placeholderTextColor={colors.PLACEHOLDER_TEXT}
      style={styles.inputContainerStyle}
      containerStyle={styles.inputContainer}
    />
));

export default DefaultInput
