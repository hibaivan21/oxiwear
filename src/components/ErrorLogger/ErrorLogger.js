import React, {Component} from 'react';
import {View} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';

import styles from './styles';

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidUpdate() {
    const {typeError, error, clearError} = this.props;
    if (error !== null) {
      this.dropDownAlertRef.alertWithType(
        typeError,
        typeError.toUpperCase(),
        error.message,
      );
      clearError();
    }
  }

  render() {
    const {children} = this.props;
    return (
      <View style={styles.container}>
        {children}
        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          updateStatusBar={false}
          containerStyle={styles.containerError}
        />
      </View>
    );
  }
}

export default index;
