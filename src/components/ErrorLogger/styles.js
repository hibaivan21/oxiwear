import {StyleSheet, Platform} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  containerError: {
    paddingTop: Platform.OS === 'android' ? 20 : 40,
  },
});
