import ErrorLogger from './ErrorLogger';

import {connect} from 'react-redux';
import {errorAction} from '../../redux/error';

const mapStateToProps = state => ({
  typeError: state.errorLogger.typeError,
  error: state.errorLogger.error,
});

const mapDispatchToProps = dispatch => ({
  clearError: () => dispatch(errorAction.setError('error', null)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorLogger);
