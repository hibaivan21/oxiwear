import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, ScrollView } from 'react-native';

import { MInput } from '../../../components/Inputs';
import { MButton } from '../../../components/Buttons';

import styles from './styles';
import { colors, icons } from '../../../constants';

const ItemReport = ({ item, onPressItem }) => (
  <View style={
    item.id === 5 ? styles.itemReportLast
      : styles.itemReport}>
    <Text >{item.date}</Text>
    <Text >{item.time}</Text>
    <View style={
      item.SPO2 < 90 ? styles.spo2Red 
      : item.SPO2 === 90 ? styles.spo2Yellow
      : styles.spo2Green}>
        <Text style={{color: 'white'}}>{item.SPO2}</Text></View>
    <Text style={{paddingHorizontal: 15}}>{item.HR}</Text>
  </View>
);

const ItemReportFormat = ({ item, onPressItem }) => (
  <TouchableOpacity
    style={
      item.active
        ? styles.itemReportFormatActive
        : styles.itemReportFormatDisable
    }
    onPress={() => onPressItem(item.id)}>
    <item.icon />
    <Text
      style={
        item.active
          ? styles.itemReportFormatTextActive
          : styles.itemReportFormatText
      }>
      {item.title}
    </Text>
  </TouchableOpacity>
);

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueOfReports: [
        { id: 0, date: '00/00/00', time: '00:00:00', SPO2: 88, HR: 93 },
        { id: 1, date: '00/00/00', time: '00:00:00', SPO2: 94, HR: 93 },
        { id: 2, date: '00/00/00', time: '00:00:00', SPO2: 90, HR: 93 },
        { id: 3, date: '00/00/00', time: '00:00:00', SPO2: 88, HR: 93 },
        { id: 4, date: '00/00/00', time: '00:00:00', SPO2: 88, HR: 93 },
        { id: 5, date: '00/00/00', time: '00:00:00', SPO2: 88, HR: 93 }
      ],
      reportFormat: [
        { id: 0, title: 'Excel', icon: icons.Excel, active: false },
        { id: 1, title: 'PDF', icon: icons.PDF, active: false },
      ],
    };
  }

  handlePressItem = index => {
    const { valueOfReports } = this.state;

    const newFilters = valueOfReports.map(item =>
      item.id === index ? { ...item, active: !item.active } : { ...item },
    );

    this.setState({
      valueOfReports: newFilters,
    });
  };
  handlePressItemFormat = index => {
    const { reportFormat } = this.state;

    const newFilters = reportFormat.map(item =>
      item.id === index ? { ...item, active: !item.active } : { ...item },
    );

    this.setState({ reportFormat: newFilters });
  };

  render() {
    const { valueOfReports, reportFormat } = this.state;
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.headerText}>Timeframe</Text>
        <View style={styles.timeframe}>
          <MInput label="From" placeholder="00/00/00" />
          <View style={styles.divider} />
          <MInput label="To" placeholder="00/00/00" />
        </View>
        <View>
          <Text style={styles.headerText}>Include in Report</Text>
          <View style={styles.itemReportActive}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', paddingTop: 7 }}>
              <Text>Date</Text>
              <Text>Time</Text>
              <Text>SPO2</Text>
              <Text>HR</Text>
            </View>

          </View>
          <FlatList
            data={valueOfReports}
            renderItem={({ item }) => (
              <ItemReport item={item} onPressItem={this.handlePressItem} />
            )}
            keyExtractor={item => item.id.toString()} />
        </View>
        <View>
          <Text style={styles.headerText}>Report Format</Text>
          <FlatList
            horizontal
            data={reportFormat}
            renderItem={({ item }) => (
              <ItemReportFormat
                item={item}
                onPressItem={this.handlePressItemFormat}
              />
            )}
            keyExtractor={item => item.id.toString()}
          />
        </View>
        <MButton title="Export" containerStyle={{ marginVertical: 23 }} />
      </ScrollView>
    );
  }
}

export default Report;
