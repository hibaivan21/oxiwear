import { StyleSheet } from 'react-native';

import { fonts, colors } from '../../../constants';

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    paddingHorizontal: '5%',
    paddingTop: '3.5%',
  },
  headerText: {
    marginTop: 15,
    fontFamily: fonts.robotoBold,
    fontSize: 18,
    color: colors.HEADER_COLOR,
    paddingBottom: 15
  },
  timeframe: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 7,
  },
  divider: {
    marginTop: 10,
    width: 15,
    borderWidth: 0.5,
    marginHorizontal: 5,
    borderColor: colors.BORDER,
  },
  itemReportActive: {
    flex: 1,
    height: 35,
    marginHorizontal: '3%',
    borderWidth: 1,
    borderColor: '#707070',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#EFF3FA'
  },
  itemReport: {
    flex: 1,
    height: 25,
    marginHorizontal: '3%',
    borderWidth: 1,
    borderColor: '#707070',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  itemReportLast: {
    flex: 1,
    height: 25,
    marginHorizontal: '3%',
    borderWidth: 1,
    borderColor: '#707070',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  itemReportDisable: {
    flex: 1,
    height: 25,
    marginHorizontal: '3%',
    borderWidth: 1,
    borderColor: '#707070',
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#EFEFEF'
  },
  itemReportLastDisable: {
    flex: 1,
    height: 25,
    marginHorizontal: '3%',
    borderWidth: 1,
    borderColor: '#707070',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#EFEFEF'
  },
  itemReportText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: colors.HEADER_COLOR,
  },
  itemReportTextActive: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: 'white',
  },
  itemReportFormatActive: {
    width: 100,
    flexDirection: 'row',
    flex: 1,
    borderWidth: 0.5,
    height: 45,
    borderColor: 'silver',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    margin: 4.5,
    backgroundColor: colors.MAIN_GRADIENT_START,
  },
  itemReportFormatDisable: {
    flexDirection: 'row',
    flex: 1,
    width: 100,
    borderWidth: 0.5,
    height: 45,
    borderColor: 'silver',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    margin: 4.5,
  },
  itemReportFormatText: {
    marginLeft: 15,
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: colors.HEADER_COLOR,
  },
  itemReportFormatTextActive: {
    marginLeft: 15,
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: 'white',
  },
  spo2Red: {
    backgroundColor: '#F73B3B',
    marginVertical: 2.5, 
    width: 34,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  spo2Green: {
    backgroundColor: '#2FC67B',
    marginVertical: 2.5, 
    width: 34,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  spo2Yellow: {
    backgroundColor: '#F7B500',
    marginVertical: 2.5, 
    width: 34,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
