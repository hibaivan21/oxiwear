import React, {Component} from 'react';
import {View, Text, ScrollView, FlatList} from 'react-native';
import {Input} from 'react-native-elements';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';

import {DefaultButton} from '../../../components/Buttons';

import styles from './styles';
import {fonts} from '../../../constants';

const ProfileItem = ({title, value}) => (
  <View style={styles.profileItemContainer}>
    <Text style={styles.titleStyle}>{title}</Text>
    <Text style={styles.valueStyle}>{value}</Text>
  </View>
);

const ProfileWithManyItem = ({title, value, onPress}) => (
  <View>
    <View style={styles.profileItemContainer}>
      <Text style={styles.titleStyle}>{title}</Text>
      <Text style={styles.valueStyle} onPress={() => onPress()}>
        + add
      </Text>
    </View>
    <FlatList data={value} renderItem={({item}) => <Text>{item}</Text>} />
  </View>
);

class MedicalID extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {
        name: 'Amy Smith',
        age: 32,
        gender: 'femaly',
        weight: '130',
        phClass: 3,
        safeThreshold: '87-100',
        medication: [],
        reactions: [],
        emergencyContact: [],
        doctorNote: '',
      },
    };
  }

  handleAddNotes = value => {
    this.setState({
      profile: {
        ...this.state.profile,
      },
    });
  };

  render() {
    const {profile} = this.state;
    return (
      <KeyboardAvoidingScrollView contentContainerStyle={styles.container}>
        <ProfileItem title="Name" value={profile.name} />
        <ProfileItem title="Age" value={profile.age} />
        <ProfileItem title="Gender" value={profile.gender} />
        <ProfileItem title="Weight" value={profile.weight} />
        <ProfileItem title="PH Class" value={profile.phClass} />
        <ProfileItem title="Safe Threshold" value={profile.safeThreshold} />
        <ProfileWithManyItem
          title="Medication"
          value={profile.medication}
          onPress={() => {}}
        />
        <ProfileWithManyItem
          title="Allergies/Reactions"
          value={profile.reactions}
          onPress={() => {}}
        />
        <ProfileWithManyItem
          title="Emergency Contacts"
          value={profile.emergencyContact}
          onPress={() => {}}
        />

        <View style={{paddingHorizontal: 20, paddingTop: 10}}>
          <Text style={styles.titleStyle}>Docto'r Notes</Text>
          <Input
            multiline
            placeholder="tap to add notes"
            inputStyle={{fontSize: 14, fontFamily: fonts.robotoRegular}}
            inputContainerStyle={{borderBottomWidth: 0}}
            containerStyle={{paddingHorizontal: 0}}
          />
        </View>
        <DefaultButton title="START" containerStyle={{marginVertical: 20}} />
      </KeyboardAvoidingScrollView>
    );
  }
}

export default MedicalID;
