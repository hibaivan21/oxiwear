import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { Icon, Input } from 'react-native-elements';

import { colors } from '../../../constants';
import styles from './styles';

const ItemMedication = ({ item }) => (
  <View style={styles.itemItemMedicationContainer}>
    <View>
      <Text style={styles.itemName}>{item.name}</Text>
      <Text style={styles.itemSName}>{item.sName}</Text>
    </View>
    <Icon
      underlayColor="transparent"
      name="ios-add"
      type="ionicon"
      color={colors.REGULAR_COLOR}
      size={32}
      onPress={() => null}
    />
  </View>
);

class Medications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionsList: [
        {
          id: 0,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 1,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 2,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 3,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 4,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 0,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 1,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 2,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 3,
          name: 'Medication',
          sName: 'dosage',
        },
        {
          id: 4,
          name: 'Medication',
          sName: 'dosage',
        },
      ],
    };
  }
  render() {
    return (
      <View style={styles.container}>
        {/* <View style={styles.search}>
          <Input
            placeholder="Search for a medication"
            leftIcon={{
              name: 'magnify',
              type: 'material-community',
              color: colors.REGULAR_COLOR,
            }}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={styles.containerStyle}
          />
        </View> */}
        <View style={styles.headerContainer}>
          <View style={styles.headerFilter}>
            <Text style={styles.headerTitle}>History</Text>
            <View style={styles.filterContainer}>
              <Icon name="filter-variant" type="material-community" />
              <Text style={styles.labelText}>Most Recent</Text>
            </View>
          </View>
        </View>
        <View
          style={{ borderTopWidth: 0.5, borderColor: colors.BORDER, flex: 1 }}>
          <FlatList
            data={this.state.optionsList}
            keyExtractor={(item, index) => item.id.toString()}
            renderItem={({ item }) => <ItemMedication item={item} />}
          />
        </View>
      </View>
    );
  }
}

export default Medications;
