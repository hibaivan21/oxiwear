import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  itemItemMedicationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    backgroundColor: 'white',
    borderColor: colors.BORDER,
    alignItems: 'center',
    paddingVertical: 15,
  },
  search: {
    marginVertical: 15, 
    paddingHorizontal: '10%', 
    position: 'relative', 
    top: -40,
  },
  itemName: {
    fontSize: 16,
    fontFamily: fonts.robotoRegular,
    color: colors.REGULAR_COLOR,
  },
  itemSName: {
    fontSize: 12,
    fontFamily: fonts.robotoRegular,
    color: colors.REGULAR_COLOR,
  },
  inputStyle: {
    fontFamily: fonts.robotoMedium,
    fontSize: 12,
    color: colors.REGULAR_COLOR,
    marginLeft: 10,
  },
  inputContainerStyle: {
    backgroundColor: '#E8E8E8',
    borderRadius: 10,
    borderBottomWidth: 0
  },
  containerStyle: {
    paddingHorizontal: 0,
  },
  headerFilter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15,
  },
  headerContainer: {
    marginTop: 40,
    marginBottom: 20,
    paddingHorizontal: '10%',
    justifyContent: 'space-between',
  },
  headerTitle: {
    fontFamily: fonts.robotoBold,
    fontSize: 18,
    color: colors.REGULAR_COLOR,
  },
  labelText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 12,
  },
  filterContainer: {
    borderWidth: 0.5,
    height: 30,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    flexDirection: 'row',
  },
});
