import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import {Icon, Divider, Input} from 'react-native-elements';

import styles from './styles';
import {icons} from '../../../constants';

const SimpleBlock = ({name, value}) => (
  <View
    style={{
      flexDirection: 'row',
      flex: 1,
      alignItems: 'center',
    }}>
    <Text style={styles.label}>{name}:</Text>
    <Text style={styles.value}>{value}</Text>
  </View>
);

const JournalItem = ({item, onPressAdd}) => {
  const {data} = item;
  return (
    <KeyboardAvoidingScrollView
      style={styles.journalItemContainer}
      showsVerticalScrollIndicator={false}>
      <View style={styles.headerBlock}>
        <Icon name="arrow-back" type="ion-icon" />
        <Text style={styles.headerTextDate}>{item.date}</Text>
        <Icon name="arrow-forward" type="ion-icon" />
      </View>
      <View style={styles.mainInformation}>
        <View style={styles.row}>
          <SimpleBlock name="Date" value={data.date} />
          <SimpleBlock name="Altitude" value={data.altitude} />
        </View>
        <View style={styles.row}>
          <SimpleBlock name="Time" value={data.time} />
          <SimpleBlock name="Air Quality Index" value={data.air} />
        </View>
        <View style={styles.row}>
          <SimpleBlock name="SPO2" value={data.spo2} />
          <SimpleBlock name="Barometric Pressure" value={data.pressure} />
        </View>
        <View style={styles.row}>
          <SimpleBlock name="HR" value={data.hr} />
          <SimpleBlock name="Humidity" value={data.humidity} />
        </View>
      </View>
      <View style={styles.block}>
        <View style={styles.headerSmallBlock}>
          <icons.Pills />
          <Text style={styles.headerSamllBlockText}>Medication</Text>
          <Icon
            name="ios-add"
            type="ionicon"
            size={30}
            onPress={() => onPressAdd()}
          />
        </View>
        <FlatList
          data={data.medication}
          renderItem={({item}) => (
            <Text>
              {item.title} / {item.dosage}
            </Text>
          )}
          contentContainerStyle={{paddingHorizontal: 20, paddingTop: 10}}
          keyExtractor={item => item.id.toString()}
          ItemSeparatorComponent={Divider}
        />
      </View>
      <View style={styles.block}>
        <View style={styles.headerSmallBlock}>
          <icons.Symptoms />
          <Text style={styles.headerSamllBlockText}>Symptoms</Text>
          <Icon
            name="ios-add"
            type="ionicon"
            size={30}
            onPress={() => onPressAdd()}
          />
        </View>
        <FlatList
          data={data.symptoms}
          renderItem={({item}) => (
            <Text>
              {item.title} / {item.time}
            </Text>
          )}
          contentContainerStyle={{paddingHorizontal: 20, paddingTop: 10}}
          keyExtractor={item => item.id.toString()}
          ItemSeparatorComponent={Divider}
        />
      </View>
      <View style={styles.block}>
        <View style={styles.headerSmallBlock}>
          <icons.Notes />
          <Text style={styles.headerSamllBlockText}>Notes</Text>
          <Icon name="ios-add" type="ionicon" size={30} />
        </View>
        <Input
          multiline
          placeholder="tap to add feelings or thoughts"
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainerStyle}
        />
      </View>
    </KeyboardAvoidingScrollView>
  );
};

class Journal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      journal: [
        {
          id: 0,
          date: '18.02.2020',
          data: {
            date: '18.02.2020',
            time: '22:28',
            spo2: '98',
            hr: 87,
            altitude: '18',
            air: '20',
            pressure: '18',
            humidity: '12',
            medication: [
              {id: 0, title: 'Paracetamol', dosage: '2mlg'},
              {id: 1, title: 'Analgin', dosage: '1mlg'},
            ],
            symptoms: [
              {id: 0, title: 'Blow braine', time: '22:30'},
              {id: 1, title: 'Lose hand', time: '23:15'},
            ],
          },
        },
        {
          id: 1,
          date: '18.02.2020',
          data: {
            date: '18.02.2020',
            time: '22:28',
            spo2: '98',
            hr: 87,
            altitude: '18',
            air: '20',
            pressure: '18',
            humidity: '12',
            medication: [
              {id: 0, title: 'Paracetamol', dosage: '2mlg'},
              {id: 1, title: 'Analgin', dosage: '1mlg'},
            ],
            symptoms: [
              {id: 0, title: 'Blow braine', time: '22:30'},
              {id: 1, title: 'Lose hand', time: '23:15'},
            ],
          },
        },
      ],
    };
  }

  onPressPlus = () => {
    const {navigation} = this.props;
    navigation.navigate('SemiJournal');
  };

  render() {
    const {journal} = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal
          data={journal}
          renderItem={({item}) => (
            <JournalItem item={item} onPressAdd={() => this.onPressPlus()} />
          )}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}

export default Journal;
