import {StyleSheet, Dimensions} from 'react-native';

import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  headerTextDate: {
    fontFamily: fonts.robotoRegular,
    fontSize: 18,
    color: colors.REGULAR_COLOR,
  },
  journalItemContainer: {
    flexGrow: 1,
    width: Dimensions.get('window').width,
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  mainInformation: {
    paddingHorizontal: 20,
    marginTop: 8,
    paddingTop: 8,
    paddingBottom: 8,
    marginBottom: 10,
    borderTopWidth: 0.5,
    borderColor: colors.BORDER,
  },
  label: {
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
  },
  value: {
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
  },
  headerSmallBlock: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    height: 37,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderColor: colors.BORDER,
  },
  block: {
    borderTopWidth: 1,
    borderColor: colors.BORDER,
    paddingBottom: 10,
  },
  headerSamllBlockText: {
    flex: 1,
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    marginLeft: 10,
  },
  inputStyle: {
    fontSize: 14,
    fontFamily: fonts.robotoRegular,
    paddingHorizontal: 50,
  },
  inputContainerStyle: {
    borderBottomWidth: 0,
  },
});
