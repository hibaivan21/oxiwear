import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import {Icon, Input} from 'react-native-elements';

import {colors} from '../../../constants';
import styles from './styles';

const ItemSymptoms = ({item}) => (
  <View style={styles.itemItemMedicationContainer}>
    <View>
      <Text style={styles.itemName}>{item.name}</Text>
      <Text style={styles.itemSName}>{item.sName}</Text>
    </View>
    <Icon
      underlayColor="transparent"
      name="ios-add"
      type="ionicon"
      color={colors.REGULAR_COLOR}
      size={32}
      onPress={() => null}
    />
  </View>
);

class Symptoms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionsList: [
        {
          id: 0,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 1,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 2,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 3,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 4,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 5,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 6,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 7,
          name: 'Symptom',
          sName: 'times added',
        },
        {
          id: 8,
          name: 'Symptom',
          sName: 'times added',
        },
      ],
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.headerFilter}>
            <Text style={styles.headerTitle}>History</Text>
            <View style={styles.filterContainer}>
              <Icon name="filter-variant" type="material-community" />
              <Text style={styles.labelText}>Most Recent</Text>
            </View>
          </View>
        </View>
        <View
          style={{borderTopWidth: 0.5, borderColor: colors.BORDER, flex: 1}}>
          <FlatList
            data={this.state.optionsList}
            keyExtractor={(item, index) => item.id.toString()}
            renderItem={({item}) => <ItemSymptoms item={item} />}
          />
        </View>
      </View>
    );
  }
}

export default Symptoms;
