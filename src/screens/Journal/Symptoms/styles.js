import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  itemItemMedicationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    borderBottomWidth: 0.5,
    backgroundColor: 'white',
    borderColor: colors.BORDER,
    alignItems: 'center',
    paddingVertical: 15,
  },
  itemName: {
    fontSize: 16,
    fontFamily: fonts.robotoRegular,
    color: colors.REGULAR_COLOR,
  },
  itemSName: {
    fontSize: 12,
    fontFamily: fonts.robotoRegular,
    color: colors.REGULAR_COLOR,
  },
  inputStyle: {
    fontFamily: fonts.robotoMedium,
    fontSize: 12,
    color: colors.REGULAR_COLOR,
    marginLeft: 10,
  },
  inputContainerStyle: {
    backgroundColor: '#E8E8E8',
    borderRadius: 10,
    borderBottomWidth: 0,
  },
  containerStyle: {
    paddingHorizontal: 0,
  },
  headerFilter: {
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
    alignItems: 'center',
  },
  headerContainer: {
    marginVertical: 15,
    paddingHorizontal: '10%',
  },
  headerTitle: {
    fontFamily: fonts.robotoBold,
    fontSize: 18,
    color: colors.REGULAR_COLOR,
  },
  labelText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 12,
  },
  filterContainer: {
    borderWidth: 0.5,
    height: 30,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    flexDirection: 'row',
  },
});
