import React, {Component} from 'react';
import {View, Text, FlatList, Dimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import {getBatteryLevel} from 'react-native-device-info';

import {colors} from '../../constants';
import styles from './styles';

class PowerLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modules: [
        {id: 0, title: 'Oxygen', value: '409', type: 'percent'},
        {id: 1, title: 'Heart Rate', value: '93', type: 'bpm'},
      ],
    };
  }

  componentDidMount() {
    getBatteryLevel().then(batteryLevel => {
      this.setState({bLevel: batteryLevel});
    });
  }

  render() {
    const {bLevel} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View style={{paddingHorizontal: 10, paddingTop: 20}}>
          <FlatList
            numColumns={2}
            data={this.state.modules}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <View style={styles.itemContainer}>
                <Text style={styles.simpleText}>{item.title}</Text>
                <Text style={styles.simpleValue}>{item.value}</Text>
                <Text style={styles.simpleText}>{item.type}</Text>
              </View>
            )}
          />
        </View>
        <View style={{paddingHorizontal: 10}}>
          <View style={[styles.itemContainer, {flex: 0}]}>
            <Text style={styles.simpleText}>Battery Level</Text>
            <ProgressBarAnimated
              width={Dimensions.get('window').width - 60}
              height={18}
              value={bLevel !== -1 ? bLevel : 80}
              backgroundColorOnComplete="#6CC644"
            />
            <View style={styles.labelStatusBar}>
              {[0, 25, 50, 75, 100].map(item => (
                <Text key={item.toString()} style={styles.labelText}>
                  {item}%
                </Text>
              ))}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default PowerLevel;
