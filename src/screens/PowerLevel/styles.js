import {StyleSheet, Dimensions} from 'react-native';
import {fonts, colors} from '../../constants';

export default StyleSheet.create({
  headerText: {
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: 'white',
  },
  simpleText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: colors.REGULAR_COLOR,
    marginBottom: 5,
  },
  simpleValue: {
    fontFamily: fonts.robotoMedium,
    fontSize: 25,
    color: '#58ACDB',
  },
  itemContainer: {
    flex: 1,
    margin: 7.5,
    backgroundColor: 'white',

    alignItems: 'center',
    height: 84,
    borderRadius: 20,
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  header: {
    height: 170,
    justifyContent: 'flex-end',
    padding: 20,
  },
  labelText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    marginVertical: 5,
    color: colors.REGULAR_COLOR,
  },
  labelStatusBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Dimensions.get('window').width - 60,
  },
});
