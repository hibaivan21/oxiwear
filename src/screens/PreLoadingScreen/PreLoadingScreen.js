import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {Wear} from '../../constants/images';

import {fonts} from '../../constants';

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  

  render() {
    const {navigation} = this.props
    setTimeout(() => {
      navigation.navigate('Auth')
    }, 3000);
    return (
      <View style={styles.container}>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="light-content"
        />
        <View style={styles.sliderBlock}>
          <LinearGradient
            colors={['#3E3E3E', '#000000']}
            style={styles.gradientContainer}>
            <SafeAreaView style={styles.containerSwiper}>
              <View style={styles.header}>
                <Wear width={width-100} height={width-100} />
              </View>
            </SafeAreaView>
          </LinearGradient>
        </View>

      </View>
    );
  }
}

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    alignItems: 'center',
    marginTop: 20,
  },
  sliderBlock: {
    height: '100%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 0,

    elevation: 3,
  },
  bottomContainer: {
    flex: 2,
    paddingBottom: 20,
  },
  gradientContainer: {
    flex: 1,
    zIndex: 6,shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    
    elevation: 10,
  },
  contentBlock: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    zIndex: 6,
  },
  footerText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: 'white',
    marginVertical: 20,
    textAlign: 'center',
  },
  learnMore: {
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    color: 'white',
    marginVertical: 15,
    textAlign: 'center',
  },
  containerSwiper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  paginationContainer: {
    paddingTop: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  activePag: {
    width: 14,
    height: 14,
    borderRadius: 20,
    backgroundColor: '#373737',
    marginHorizontal: 3,
  },
  unactivePag: {
    width: 14,
    height: 14,
    borderRadius: 20,
    backgroundColor: '#F7F7F7',
    marginHorizontal: 3,
  },
});

export default Auth;
