import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../constants';

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
  },
  timerLabel: {
    fontFamily: fonts.robotoBold,
    fontSize: 30,
    color: colors.REGULAR_COLOR,
    textAlign: 'center',
  },
  chartContainer: {
    backgroundColor: 'white',
    marginHorizontal: 18,
    marginBottom: 18,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  headerLabel: {
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: colors.REGULAR_COLOR,
  },
  smallLabel: {
    fontFamily: fonts.robotoRegular,
    fontSize: 12,
  },
  headerLabelBlock: {
    marginHorizontal: 18,
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
