import React, {Component} from 'react';
import {View, Text, Dimensions, ScrollView} from 'react-native';
import {LineChart} from 'react-native-chart-kit';

import {MButton} from '../../components/Buttons';

import styles from './styles';
import {colors} from '../../constants';

class Progress extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.headerLabelBlock}>
          <Text style={styles.headerLabel}>
            SPO2 95 <Text style={styles.smallLabel}>percent</Text>
          </Text>
          <Text>
            <Text style={styles.headerLabel}>
              HR 93 <Text style={styles.smallLabel}>bpm</Text>
            </Text>
          </Text>
        </View>
        <View style={styles.chartContainer}>
          <LineChart
            bezier
            withDots={false}
            withShadow={false}
            withInnerLines={false}
            data={{
              labels: ['0', '1', '2', '3', '4', '5', '6'],
              datasets: [
                {
                  data: [
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                  ],
                  color: (opacity = 1) => '#29B4DC',
                },
              ],
            }}
            width={Dimensions.get('window').width - 36} // from react-native
            height={300}
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: '#e26a00',
              backgroundGradientFrom: '#FFFFFF',
              backgroundGradientTo: '#FFFFFF',
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => 'silver',
              labelColor: (opacity = 1) => '#29B4DC',
            }}
          />
        </View>
        <View style={{flex: 1}}>
          <Text style={styles.timerLabel}>6:00</Text>
          <View style={{flexDirection: 'row'}}>
            <MButton
              title="RESET"
              buttonTitle={{color: colors.REGULAR_COLOR}}
              buttonStyle={{backgroundColor: '#E8E8E8'}}
              containerStyle={{flex: 1}}
            />
            <MButton title="START" containerStyle={{flex: 1}} />
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default Progress;
