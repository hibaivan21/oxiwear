import SingIn from './SingIn';

import {connect} from 'react-redux';
import {authAction} from '../../../redux/auth';
import {errorAction} from '../../../redux/error';

const mapStateToProps = state => {
  return {
    success: state.auth.success,
    loading: state.auth.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signIn: data => dispatch(authAction.signIn(data)),
    setError: (type, error) => dispatch(errorAction.setError(type, error)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SingIn);
