import React, { Component } from 'react';
import { View, Text, Platform, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view';
import Logo from '../../../assets/images/logoRound.svg';

import { DefaultButton } from '../../../components/Buttons';
import { DefaultInput } from '../../../components/Inputs/';

import styles from './styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class SingIn extends Component {
  constructor(props) {
    super(props);
    this.password = React.createRef();
    this.state = {
      email: null,
      password: null,
    };
  }

  handlePressSignIn = () => {
    const { email, password } = this.state;
    const { setError, navigation } = this.props;

    if (email === null && password === null) {
      setError('warn', {
        message: 'Please fill required filers below, before continue',
      });
    } else {
      navigation.navigate('DeviceConnection');
    }
  };

  handlePressCreateAccount = () => {
    const { navigation } = this.props;

    navigation.navigate('SignUp');
  };

  onChangeState = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  render() {
    const { email, password } = this.state;
    return (
      <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={styles.container}>
          <LinearGradient
            colors={['#3E3E3E', '#000000']}
            style={styles.gradientContainer}>
            <View style={styles.topBlock}>
              <Logo width={176} height={176} />
            </View>
            <Text style={styles.headerText}>Log in</Text>
            <View style={{ flex: 1 }}>
              <View style={{ marginVertical: 5 }}>
                <DefaultInput
                  placeholder="email"
                  value={email}
                  onChangeText={text => this.onChangeState('email', text)}
                  blurOnSubmit={false}
                  returnKeyType={"next"}
                  onSubmitEditing={() => this.password.current.focus()}
                />
              </View>
              <View style={{ marginVertical: 5 }}>
                <DefaultInput
                  placeholder="password"
                  secureTextEntry
                  value={password}
                  onChangeState={text => this.onChangeState('password', text)}
                  ref={this.password}
                />
              </View>
            </View>
          </LinearGradient>
        </View>
        <View style={{ flex: 2 }}>
          <Text
            style={styles.textFooter}
            onPress={this.handlePressCreateAccount}>
            I don't have an account
          </Text>
          <DefaultButton title="START" onPress={this.handlePressSignIn} />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

export default SingIn;
