import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  Platform,
  SafeAreaView,
  Text,
  FlatList,
  Dimensions,
} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';

import {Logo} from '../../constants/images';
import SwiperBlock from '../../components/SwiperBlock';
import {DefaultButton} from '../../components/Buttons';

import * as staticData from '../../constants/staticData';
import {colors, fonts} from '../../constants';

const PaginationItem = ({index, currentIndex}) => (
  <View
    style={index === currentIndex ? styles.activePag : styles.unactivePag}
  />
);

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };
  }

  setIndex = index => {
    this.setState({currentIndex: index.index});
  };

  render() {
    const {currentIndex} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="light-content"
        />

        <View style={styles.sliderBlock}>
          <LinearGradient
            colors={['#3E3E3E', '#000000']}
            style={styles.gradientContainer}>
            <SafeAreaView style={styles.containerSwiper}>
              <View style={styles.header}>
                <Logo width={293} height={69} />
              </View>

              <View style={styles.contentBlock}>
                <SwiperFlatList
                  //autoplay
                  //autoplayDelay={3}
                  //autoplayLoop
                  //showPagination
                  data={staticData.authData}
                  renderItem={({item}) => <SwiperBlock item={item} />}
                  containerStyle={{flexGrow: 1}}
                  onChangeIndex={index => this.setIndex(index)}
                  //paginationStyle={{marginBottom: -55}}
                  //paginationActiveColor={colors.ACTIVE_PAGINATION}
                  //paginationDefaultColor={colors.UNACTIVE_PAGINATION}
                />
              </View>
              <Text style={styles.learnMore}>Learn More</Text>
            </SafeAreaView>
          </LinearGradient>
        </View>
        <View style={styles.paginationContainer}>
          <FlatList
            horizontal
            data={[0, 1, 2, 3]}
            renderItem={({index}) => (
              <PaginationItem index={index} currentIndex={currentIndex} />
            )}
          />
        </View>

        <View style={styles.bottomContainer}>
          <DefaultButton
            title="Sign Up"
            onPress={() => this.props.navigation.navigate('SignUp')}
          />
          <DefaultButton
            title="Sign In"
            onPress={() => this.props.navigation.navigate('SignIn')}
          />
        </View>
      </View>
    );
  }
}

const height = Dimensions.get('window').height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    alignItems: 'center',
    marginTop: 20,
  },
  sliderBlock: {
    height: height*0.76,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 0,

    elevation: 3,
  },
  bottomContainer: {
    flex: 2,
    paddingBottom: 20,
  },
  gradientContainer: {
    flex: 1,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    zIndex: 6,shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    
    elevation: 10,
  },
  contentBlock: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    zIndex: 6,
  },
  footerText: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: 'white',
    marginVertical: 20,
    textAlign: 'center',
  },
  learnMore: {
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    color: 'white',
    marginVertical: 15,
    textAlign: 'center',
  },
  containerSwiper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  paginationContainer: {
    paddingTop: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  activePag: {
    width: 14,
    height: 14,
    borderRadius: 20,
    backgroundColor: '#373737',
    marginHorizontal: 3,
  },
  unactivePag: {
    width: 14,
    height: 14,
    borderRadius: 20,
    backgroundColor: '#F7F7F7',
    marginHorizontal: 3,
  },
});

export default Auth;
