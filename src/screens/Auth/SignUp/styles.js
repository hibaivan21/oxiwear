import {StyleSheet, Platform, Dimensions} from 'react-native';

import {fonts, colors} from '../../../constants';

const height = Dimensions.get('window').height

export default StyleSheet.create({
  container: {
    height: height*0.76,
    elevation: 10
  },
  gradientContainer: {
    flex: 1,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    paddingTop: Platform.OS === 'ios' ? 50 : 30,
    paddingBottom: 20,shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    
    elevation: 10,
  },
  textFooter: {
    textAlign: 'center',
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
    color: colors.TEXT_FOOTER,
    marginVertical: 8,
    textDecorationLine: 'underline',
  },
  topBlock: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    textAlign: 'center',
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: colors.UNACTIVE_PAGINATION,
    paddingBottom: 10,
  },
  termsOfUse: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 20,
    fontSize: 18,
    paddingLeft: 60,
    paddingTop: 15,
    alignItems: 'center',
  },
  termsOfUseText: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    lineHeight: 19,
    color: colors.UNACTIVE_PAGINATION,
  },
});
