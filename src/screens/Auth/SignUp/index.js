import SignUp from './SignUp';

import {connect} from 'react-redux';
import {authAction} from '../../../redux/auth';
import {errorAction} from '../../../redux/error';

const mapStateToProps = state => {
  return {
    success: state.auth.success,
    loading: state.auth.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signUp: data => dispatch(authAction.signUp(data)),
    setError: (type, message) => dispatch(errorAction.setError(type, message)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
