import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view';

import Logo from '../../../assets/images/logoRound.svg';
import { DefaultInput } from '../../../components/Inputs/';
import { DefaultButton } from '../../../components/Buttons';
import colors from '../../../constants/colors';
import styles from './styles';

class SingIn extends Component {
  constructor(props) {
    super(props);
    this.email = React.createRef();
    this.password = React.createRef();
    this.state = {
      first_name: null,
      email: null,
      password: null,
      termsCheck: false,
    };
  }

  check = () => {
    this.setState({ termsCheck: !this.state.termsCheck });
  };

  onChangeState = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  handlePressHaveAccount = () => {
    const { navigation } = this.props;

    navigation.navigate('SignIn');
  };

  handlePressSignUp = () => {
    const { first_name, email, password, termsCheck } = this.state;
    const { setError, navigation } = this.props;

    if (termsCheck) {
      if (first_name === null || email === null || password === null) {
        setError('warn', {
          message: 'Please fill required filers below, before continue',
        });
      } else {
        navigation.navigate('DeviceConnection');
      }
    } else {
      setError('warn', {
        message: 'First agree Terms of Use',
      });
    }
  };

  render() {
    const { termsCheck, email, password, first_name } = this.state;
    return (
      <KeyboardAwareScrollView
        scrollEnabled
        contentContainerStyle={{ flexGrow: 1 }}>
        <View style={styles.container}>
          <LinearGradient
            colors={['#3E3E3E', '#000000']}
            style={styles.gradientContainer}>
            <View style={styles.topBlock}>
              <Logo width={176} height={176} />
            </View>
            <Text style={styles.headerText}>Sign Up</Text>
            <View style={{ flex: 1 }}>
              <View style={{ marginVertical: 5 }}>
                <DefaultInput
                  placeholder="first name"
                  value={first_name}
                  onChangeText={text => this.onChangeState('first_name', text)}
                  blurOnSubmit={false}
                  returnKeyType={"next"}
                  onSubmitEditing={() => this.email.current.focus()}                />
              </View>
              <View style={{ marginVertical: 5 }}>
                <DefaultInput
                  placeholder="email"
                  keyboardType='email-address'
                  value={email}
                  onChangeText={text => this.onChangeState('email', text)}
                  ref={ this.email}
                  blurOnSubmit={false}
                  returnKeyType={"next"}
                  onSubmitEditing={() => this.password.current.focus()}
                />
              </View>
              <View style={{ marginVertical: 5 }}>
                <DefaultInput
                  secureTextEntry
                  placeholder="password"
                  value={password}
                  ref={this.password}
                  onChangeText={text => this.onChangeState('password', text)}
                />
              </View>

              <View style={styles.termsOfUse}>
                <CheckBox
                  onPress={this.check}
                  checked={termsCheck}
                  uncheckedIcon={
                    <Icon
                      name="square-outline"
                      type="material-community"
                      size={1}
                      color="transparent"
                    />
                  }
                  checkedIcon={
                    <Icon
                      name="check"
                      type="font-awesome"
                      color={colors.BUTTON_BACKGROUND}
                      size={20}
                    />
                  }
                  textStyle={{
                    textAlign: 'center',
                  }}
                  containerStyle={{
                    padding: 0,
                    width: 20,
                    height: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'white',
                  }}
                />
                <View>
                  <Text style={styles.termsOfUseText}>
                    I agree to the OxyWear {''}
                    <Text
                      style={[
                        styles.termsOfUseText,
                        { textDecorationLine: 'underline' },
                      ]}>
                      {terms}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </LinearGradient>
        </View>

        <View style={{ flex: 2 }}>
          <Text style={styles.textFooter} onPress={this.handlePressHaveAccount}>
            I already have an account
          </Text>
          <DefaultButton title="START" onPress={this.handlePressSignUp} />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

export default SingIn;

const terms = 'Terms of\n Use, Data and Privacy Policy'

