import React, {Component} from 'react';
import {View, Text, Alert} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Wear} from '../../../constants/images';

import {MButton, DefaultButton} from '../../../components/Buttons';

import styles from './styles';

const makeSureText = 'your bluetooth is on and \nyour OxiWear device is on and \nclose by'

class SingIn extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  alert = () => {
    Alert.alert(
      "A Short Title Is Best",
      "A message should be a short, complete sentence.",
      [
        { text: "Action Preferred", onPress: () => this.props.navigation.navigate('ConnectionProcess') }
      ]
    );
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <LinearGradient
            colors={['#3E3E3E', '#000000']}
            style={styles.gradientContainer}>
            <Wear width={176} height={176} />

            <Text style={styles.makeSureText}>
              {makeSureText}
            </Text>
          </LinearGradient>
        </View>
        <View style={{flex: 2}}>
          <Text style={styles.textFooter}>
            Press “Start” to set up your device!
          </Text>
          <DefaultButton
            title="START"
            onPress={() => this.alert()}
          />
          <DefaultButton
            title="BLE test"
            onPress={() => this.props.navigation.navigate('Connection')}
          />
        </View>
      </View>
    );
  }
}

export default SingIn;
