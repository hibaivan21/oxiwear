import {StyleSheet, Dimensions} from 'react-native';

import {fonts, colors} from '../../../constants';

const height = Dimensions.get('window').height

export default StyleSheet.create({
  container: {
    height: height*0.76
  },
  gradientContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    
    elevation: 10,
  },
  textFooter: {
    textAlign: 'center',
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    color: colors.TEXT_FOOTER,
    margin: 10,
  },
  makeSureText: {
    marginTop: 60,
    paddingHorizontal: 60,
    textAlign: 'center',
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    lineHeight: 20,
    color: colors.UNACTIVE_PAGINATION,
  },
});
