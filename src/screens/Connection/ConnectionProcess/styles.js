import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  gradientContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    
    elevation: 10,
  },
  makeSureText: {
    marginTop: 60,
    paddingHorizontal: 60,
    textAlign: 'center',
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    lineHeight: 20,
    color: colors.UNACTIVE_PAGINATION,
  },
});
