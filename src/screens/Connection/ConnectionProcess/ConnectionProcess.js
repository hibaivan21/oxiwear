import React, {Component} from 'react';
import {View, Text, Image, Animated, Easing, Alert } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {Wear, images} from '../../../constants/images';

import styles from './styles';

class ConnectionProcess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 74,
      height: 64,
    };
    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.animate();
    setTimeout(() => {
      this.props.navigation.navigate('DeviceConnected');
    }, 3000);
 }

  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
    }).start(() => this.animate());
  }

  render() {
    const widthImage = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 100],
    });
    const heightImage = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 100],
    });

    return (
      <LinearGradient
        colors={['#3E3E3E', '#000000']}
        style={styles.gradientContainer}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Animated.Image
            style={{width: widthImage, height: heightImage}}
            source={images.LeftBLuetooth}
            resizeMode={'contain'}
          />
          <Wear width={211} height={209} />
          <Animated.Image
            style={{width: widthImage, height: heightImage}}
            source={images.RightBLuetooth}
            resizeMode={'contain'}
          />
        </View>
        <Text style={styles.makeSureText}>Connecting to your device</Text>
      </LinearGradient>
    );
  }
}

export default ConnectionProcess;

