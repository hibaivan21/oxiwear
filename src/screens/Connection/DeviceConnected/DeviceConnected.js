import React, {Component} from 'react';
import {View, Text, Alert} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Wear} from '../../../constants/images';

import {DefaultButton} from '../../../components/Buttons';

import styles from './styles';

class DeviceConnected extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  alert = () => {
    Alert.alert(
      "A Short Title Is Best",
      "A message should be a short, complete sentence.",
      [
        { text: "Action Preferred", onPress: () => this.props.navigation.navigate('CreateProfile') }
      ]
    );
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 8}}>
          <LinearGradient
            colors={['#3E3E3E', '#000000']}
            style={styles.gradientContainer}>
            <Wear width={202} height={200} />

            <Text style={styles.textFooter}>Connected</Text>
            <Text style={styles.makeSureText}>
              The following questions will help us customize your OxiWear
              experience
            </Text>
          </LinearGradient>
        </View>
        <View style={{flex: 2, justifyContent: 'center'}}>
          <DefaultButton
            title="START"
            onPress={() => this.alert()}
          />
        </View>
      </View>
    );
  }
}

export default DeviceConnected;
