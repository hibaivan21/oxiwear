import {StyleSheet} from 'react-native';

import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  gradientContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    
    elevation: 10,
  },
  textFooter: {
    textAlign: 'center',
    fontFamily: fonts.robotoBold,
    fontSize: 16,
    color: colors.UNACTIVE_PAGINATION,
    margin: 10,
  },
  makeSureText: {
    marginTop: 60,
    lineHeight: 24,
    paddingHorizontal: 60,
    textAlign: 'center',
    fontFamily: fonts.robotoRegular,
    fontSize: 18,
    lineHeight: 20,
    color: colors.UNACTIVE_PAGINATION,
  },
});
