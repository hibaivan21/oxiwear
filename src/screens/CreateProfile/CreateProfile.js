import React, {Component} from 'react';
import {View, Text, FlatList, SafeAreaView, StatusBar} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {DefaultButton} from '../../components/Buttons';

import styles from './styles';

import {
  ScreenSex,
  ScreenBorn,
  ScreenWeight,
  ScreenCardiovascular,
  ScreenWHO,
  ScreenSPO2,
} from './screens';

const Block = ({item}) => (
  <View style={styles.blockContainer}>
    <SafeAreaView style={{flex: 1}}>
        <Text style={styles.headerText}>{item.headerTitle}</Text>
      <View style={{flex: 1, justifyContent: 'space-around'}}>
        {item.children}
        <Text style={styles.footerText}>{item.footerText}</Text>
      </View>
    </SafeAreaView>
  </View>
);
class CreateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeScreen: 0,
      profile: {
        sex: null,
        month: null,
        year: null,
        weight: null,
      },
      screens: [
        {
          step: 1,
          headerTitle: 'Select your Sex',
          footerText:
            'Aenean purus ante, molestie \nmalesuada urna eget, molestie \nvulputate ipsum',
          children: <ScreenSex />,
        },
        {
          step: 2,
          headerTitle: 'When were you born?',
          footerText:
            'Aenean purus ante, molestie \nmalesuada urna eget, molestie \nvulputate ipsum',
          children: <ScreenBorn />,
        },
        {
          step: 3,
          headerTitle: 'What is your weight?',
          footerText:
            'Aenean purus ante, molestie \nmalesuada urna eget, molestie \nvulputate ipsum',
          children: <ScreenWeight />,
        },
        {
          step: 4,
          headerTitle: 'Do you have a \ncardiovascular disease?',
          footerText:
            'Aenean purus ante, molestie \nmalesuada urna eget, molestie \nvulputate ipsum',
          children: <ScreenCardiovascular />,
        },
        {
          step: 5,
          headerTitle: 'Do you have pulmonary hypertension (PH)?',
          footerText:
            'Aenean purus ante, molestie malesuada urna eget, molestie vulputate ipsum',
          children: <ScreenCardiovascular />,
        },
        {
          step: 6,
          headerTitle: 'What World Health \nOrganization class is your PH?',
          children: <ScreenWHO />,
        },
        {
          step: 7,
          headerTitle: 'Set your safe SPO2 threshold',
          children: <ScreenSPO2 />,
        },
      ],
    };
  }

  onChangeProfileField = (field, value) => {
    this.setState({
      [field]: value,
    });
  };

  handlePressSkip = () => {
    const {navigation} = this.props;

    navigation.navigate('App');
  };

  handlePressNext = () => {
    const {activeScreen} = this.state;
    const {navigation} = this.props;

    if (activeScreen !== 6) {
      this.screenList.scrollToIndex({animated: true, index: activeScreen + 1});
    } else {
      navigation.navigate('App');
    }
  };

  onViewableItemsChanged = ({viewableItems, changed}) => {
    this.setState({activeScreen: viewableItems[0].index});
  };

  handlePressBack = () => {
    const {activeScreen} = this.state;

    if (activeScreen !== 0) {
      this.screenList.scrollToIndex({animated: true, index: activeScreen - 1});
    }
  };

  render() {
    const {screens} = this.state;
    return (
      <LinearGradient
        colors={['#3E3E3E', '#000000']}
        style={styles.gradientContainer}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar
            barStyle="light-content"
            backgroundColor="transparent"
            translucent
          />
          <View style={styles.topNav}>
            <Text style={styles.backSkip} onPress={this.handlePressBack}>
              back
            </Text>
            <Text style={styles.backSkip}>{this.state.activeScreen + 1}/7</Text>
            <Text style={styles.backSkip} onPress={this.handlePressSkip}>
              skip
            </Text>
          </View>
          <FlatList
            ref={ref => (this.screenList = ref)}
            horizontal
            pagingEnabled
            data={screens}
            renderItem={({item, index}) => <Block item={item} />}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{flexGrow: 1}}
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={{
              itemVisiblePercentThreshold: 50,
            }}
          />
          <DefaultButton
            title="START"
            onPress={this.handlePressNext}
            containerStyle={{marginBottom: '16%'}}
          />
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

export default CreateProfile;
