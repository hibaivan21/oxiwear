import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  sexText: {
    position: 'absolute',
    bottom: -25,
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
    color: 'white',
  },
  sexButton: {
    marginHorizontal: 27,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  inputStyle: {
    textAlign: 'center',
    fontFamily: fonts.robotoMedium,
    fontSize: 14,
    width: 100,
    paddingHorizontal: 4
  },
  inputContainet: {
    height: 42,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: 'white',
  },

  buttonTitle: {
    fontFamily: fonts.robotoRegular,
    fontSize: 18,
    color: '#585858',
  },
  buttonStyle: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    height: 42,
    width: 95,
    marginHorizontal: 5,
  },
  largeButtonStyle: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    height: 42,
    width: '100%',
    alignItems: 'center',
    marginVertical: 5,
  },
  buttonLTitle: {
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
    color: colors.BUTTON_TITLE,
  },
  buttonLSecTitle: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    color: colors.BUTTON_TITLE,
  },
});
