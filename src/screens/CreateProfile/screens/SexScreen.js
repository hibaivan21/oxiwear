import React, {Component} from 'react';
import {View, TouchableOpacity, Text, Alert} from 'react-native';

import {Male, Female} from '../../../constants/images';
import styles from './styles';

const SexScreen = ({onSetProfileField}) => (
  <View style={styles.container}>
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 30,
      }}>
      <TouchableOpacity
        style={styles.sexButton}
        onPress={() => {}}>
        <Male style={{marginBottom: -2}} width={100} heigth={100} />
        <Text style={styles.sexText}>Male</Text>
      </TouchableOpacity>
      <TouchableOpacity 
        style={styles.sexButton}
        onPress={() => {}}>
        <Female style={{marginBottom: -2}} width={100} heigth={100} />
        <Text style={styles.sexText}>Female</Text>
      </TouchableOpacity>
    </View>
    <Text
      style={{
        fontSize: 16,
        textDecorationLine: 'underline',
        color: 'white',
      }}>
      Prefer not to Specify
    </Text>
  </View>
);

export default SexScreen;
