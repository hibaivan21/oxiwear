import React, { useState } from 'react';
import {View, TouchableOpacity, Text, Dimensions, StyleSheet} from 'react-native';
import {Input, Button, Divider} from 'react-native-elements';
import SwitchSelector from 'react-native-switch-selector';

import {colors, fonts} from '../../../constants';
import styles from './styles';

import ScreenSex from './SexScreen';
import { TextInput } from 'react-native-gesture-handler';

const ScreenBorn = ({}) => (
  <View style={styles.container}>
    <View
      style={{
        width: Dimensions.get('window').width,
      }}>
      <View
        style={{
          paddingHorizontal: 70,
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Input
          placeholder={'Month'}
          containerStyle={{flex: 4, paddingHorizontal: 5}}
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainet}
        />
        <Input
          placeholder={'Year'}
          containerStyle={{flex: 3, paddingHorizontal: 5}}
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainet}
        />
      </View>
    </View>
  </View>
);


const ScreenWeight = ({}) => {
  const [test, setTest] = useState('lbs')

 return (
 <View style={styles.container}>
    <View style={{marginBottom: 25, width: 100, flexDirection:'row'}}>
      <TextInput 
        keyboardType='number-pad'
        style={weightInputStyle.inpStyle}
      />
      <Text style={weightInputStyle.text}>{test}</Text>
    </View>
    <SwitchSelector
      options={[
        {label: 'lbs', value: 'lbs'},
        {label: 'kgs', value: 'kgs'},
      ]}
      initial={0}
      height={41}
      style={{paddingHorizontal: 90, marginBottom: 18}}
      buttonColor={colors.TEXT_FOOTER}
      borderRadius={12}
      textColor={'#909090'}
      onPress={value => {
        setTest(value)
      }}
    />
    <Text
      style={{fontSize: 16, textDecorationLine: 'underline', color: 'white'}}>
      Sync with Apple Health
    </Text>
  </View>
 )
  };

const ScreenCardiovascular = ({}) => (
  <View style={styles.container}>
    <View
      style={{
        width: Dimensions.get('window').width,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Button
          title={'Yes'}
          titleStyle={styles.buttonTitle}
          buttonStyle={styles.buttonStyle}
        />
        <Button
          title={'No'}
          titleStyle={styles.buttonTitle}
          buttonStyle={styles.buttonStyle}
        />
      </View>
    </View>
  </View>
);

const ScreenWHO = ({}) => (
  <View style={styles.container}>
    <View
      style={{
        width: Dimensions.get('window').width,
      }}>
      <View
        style={{
          justifyContent: 'center',
          paddingHorizontal: 35,
        }}>
        <TouchableOpacity style={styles.largeButtonStyle}>
          <Text style={styles.buttonLTitle}>Group 1</Text>
          <Text style={styles.buttonLSecTitle}>
            pulmonary arterial hypertension
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.largeButtonStyle}>
          <Text style={styles.buttonLTitle}>Group 2</Text>
          <Text style={styles.buttonLSecTitle}>
            PH due to left heart disease
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.largeButtonStyle}>
          <Text style={styles.buttonLTitle}>Group 3</Text>
          <Text style={styles.buttonLSecTitle}>
            PH due to lung disease and/or chronic hypoxia
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.largeButtonStyle}>
          <Text style={styles.buttonLTitle}>Group 4</Text>
          <Text style={styles.buttonLSecTitle}>
            PH due to blood clots in the lungs
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.largeButtonStyle}>
          <Text style={styles.buttonLTitle}>Group 5</Text>
          <Text style={styles.buttonLSecTitle}>
            PH due to blood and other disorders
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
);

const ScreenSPO2 = ({}) => (
  <View style={styles.container}>
    <View
      style={{
        width: Dimensions.get('window').width,
      }}>
      <View
        style={{
          paddingHorizontal: 70,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        
        <View style={styles.inputContainet}>
          <TextInput
          keyboardType='number-pad'
          placeholder={'enter number'}
          containerStyle={{paddingHorizontal: 5, width: 100}}
          style={[styles.inputStyle, {fontSize: 12}]}
        />
        </View>
        <Divider
          style={{
            marginHorizontal: 8,
            borderColor: '#FFFFFF',
            borderWidth: 1.5,
            borderRadius: 1,
            width: 26,
          }}
        />

        <Text
          style={{
            fontFamily: fonts.robotoMedium,
            fontSize: 30,
            color: '#ffff',
          }}>
          100%
        </Text>
      </View>
    </View>
  </View>
);
export {
  ScreenSex,
  ScreenBorn,
  ScreenWeight,
  ScreenCardiovascular,
  ScreenWHO,
  ScreenSPO2,
};

const weightInputStyle = StyleSheet.create({
  inpStyle: {
    width: 100,
    padding: 0,
    borderBottomWidth: 1, 
    borderBottomColor: '#707070',
    textAlign: 'center',
    color: '#fff',
    fontWeight: "500",
    fontSize: 18
  },
  text: {
    color: '#fff',
    fontSize: 15, 
    position:'absolute',
    bottom: 0,
    right: -30
  }
})