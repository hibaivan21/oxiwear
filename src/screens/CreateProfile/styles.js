import {StyleSheet, Dimensions} from 'react-native';

import {fonts, colors} from '../../constants';

export default StyleSheet.create({
  gradientContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  topNav: {
    marginTop: 30,
    paddingHorizontal: 40,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  backSkip: {
    color: colors.UNACTIVE_PAGINATION,
    fontSize: 16,
  },
  headerText: {
    paddingVertical: 50,
    top: '10%',
    textAlign: 'center',
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: 'white',
    lineHeight: 28,
  },
  footerText: {
    paddingHorizontal: 70,
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
    lineHeight: 16,
    color: colors.GRAY_TEXT,
    textAlign: 'center',
  },
  blockContainer: {
    width: Dimensions.get('window').width,
  },
});
