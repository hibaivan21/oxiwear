import {StyleSheet} from 'react-native';

import {colors, fonts} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerLabel: {
    fontFamily: fonts.robotoBold,
    fontSize: 24,
    color: 'white',
  },
  itemHealthBlock: {
    margin: 8,
    flex: 1,
    height: 80,
    borderRadius: 15,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
    justifyContent: 'space-around',
  },
  itemHealthBlock2: {
    margin: 8,
    flex: 1,
    height: 80,
    borderRadius: 15,
    backgroundColor: '#FAF7DC',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
    justifyContent: 'space-around',
  },
  smallLabel: {
    fontFamily: fonts.robotoRegular,
    fontSize: 12,
    color: 'white',
  },
  headerLabelBlock: {
    marginHorizontal: 18,
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemHHeaderLabel: {
    fontFamily: fonts.robotoMedium,
    fontSize: 14,
    color: colors.REGULAR_COLOR,
  },
  itemHValue: {
    fontFamily: fonts.robotoMedium,
    fontSize: 25,
    color: '#58ACDB',
  },
  itemHFooterLabel: {
    fontFamily: fonts.robotoMedium,
    fontSize: 14,
    color: colors.REGULAR_COLOR,
  },
  chartContainer: {
    marginTop: -40,
    marginBottom: 20,
    alignItems: 'center',
    borderRadius: 10,

    backgroundColor: 'white',
    padding: 20,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  activePeriod: {
    marginHorizontal: 8,
    borderWidth: 0.5,
    borderColor: '#29B4DC',
    paddingHorizontal: 5,
    borderRadius: 5,
  },
  textPeriod: {
    fontFamily: fonts.robotoRegular,
    fontSize: 14,
  },
mainText: {
    marginLeft: 25,
    fontSize: 18,
    fontWeight: '700',
    color: '#505050'
},
percentContainer: {
  flex: 1, 
  flexDirection: 'row', 
  justifyContent: 'center',
  paddingTop: 24
},
spo2: { 
  fontSize: 39,
  fontWeight: '700',
  color: colors.DONE_BUTTON_BACKGROUND
},
percentNumber: {
  fontSize: 39,
  fontWeight: '700',
  color: colors.HEADER_COLOR
},
percent: {
  fontSize: 19,
  color: colors.DONE_BUTTON_BACKGROUND,
  paddingTop: 20
},
sliderPercent: {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginHorizontal: 25
},
smallPercents: {
  fontSize: 14

}
});
