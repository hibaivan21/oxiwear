import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { LineChart } from 'react-native-chart-kit';
import Slider from 'react-native-slider'
import MButton from '../../../components/Buttons/MButton'

import { colors } from '../../../constants';
import styles from './styles';

const ItemHealth = ({ item }) => (
  <View style={item.id === 1 ? styles.itemHealthBlock2 : styles.itemHealthBlock}>
    <Text style={styles.itemHHeaderLabel}>{item.headerLabel}</Text>
    <Text style={styles.itemHValue}>{item.value}</Text>
    <Text style={styles.itemHFooterLabel}>{item.footerLabel}</Text>
  </View>
);

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      percent: 88,
      diagnosticList: [
        {
          id: 0,
          headerLabel: 'Altitude',
          value: 409,
          footerLabel: 'feet',
          danger: false,
        },
        {
          id: 1,
          headerLabel: 'Air Quality Index',
          value: 56,
          footerLabel: 'moderate',
          danger: false,
        },
        {
          id: 2,
          headerLabel: 'Humidity',
          value: 83,
          footerLabel: 'percent',
          danger: false,
        },
        {
          id: 3,
          headerLabel: 'Barometric Pressure',
          value: 30.43,
          footerLabel: 'inches HG',
          danger: false,
        },
      ],
      statisticPeriod: [
        { id: 0, title: 'Hour', active: true },
        { id: 1, title: 'Day', active: false },
        { id: 2, title: 'Week', active: false },
        { id: 3, title: 'Month', active: false },
      ],
    };
  }

  handlePressFilter = id => {
    const { statisticPeriod } = this.state;

    const newPeriod = statisticPeriod.map(item =>
      item.id === id ? { ...item, active: true } : { ...item, active: false },
    );
    this.setState({
      statisticPeriod: newPeriod,
    });
  };

  render() {
    const { diagnosticList } = this.state;
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: 'white' }}
        showsVerticalScrollIndicator={false}>
        <LinearGradient
          colors={[colors.HOME_MAIN_GRADIENT_START, colors.HOME_MAIN_GRADIENT_END]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{ height: 100 }}>
          <View style={styles.headerLabelBlock}>
            <Text style={styles.headerLabel}>
              SPO2 {this.state.percent} <Text style={styles.smallLabel}>percent</Text>
            </Text>
            <Text>
              <Text style={styles.headerLabel}>
                HR 93 <Text style={styles.smallLabel}>bpm</Text>
              </Text>
            </Text>
          </View>
        </LinearGradient>

        <View style={styles.chartContainer}>
          <LineChart
            bezier
            withDots={false}
            withShadow={false}
            withInnerLines={false}
            data={{
              labels: ['0', '1', '2', '3', '4', '5', '6'],
              datasets: [
                {
                  data: [
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                  ],
                  color: (opacity = 1) => '#2A78C6',
                },
              ],
            }}
            width={Dimensions.get('window').width - 40} // from react-native
            height={300}
            yAxisSuffix="bpm"
            yAxisInterval={1} // optional, defaults to 1
            yLabelsOffset={20}
            chartConfig={{
              backgroundColor: '#FFFFFF',
              backgroundGradientFrom: '#FFFFFF',
              backgroundGradientTo: '#FFFFFF',
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => 'silver',
              labelColor: (opacity = 1) => '#29B4DC',
            }}
          />
          <LineChart
            bezier
            withDots={false}
            withShadow={false}
            withInnerLines={false}
            withVerticalLabels={false}
            data={{
              labels: ['0', '1', '2', '3', '4', '5', '6'],
              datasets: [
                {
                  data: [
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                  ],
                  color: (opacity = 1) => '#29B4DC',
                },
              ],
            }}
            width={Dimensions.get('window').width - 40} // from react-native
            height={300}
            yAxisSuffix="%"
            yAxisInterval={1} // optional, defaults to 1
            yLabelsOffset={(Dimensions.get('window').width - 110) * -1}
            chartConfig={{
              backgroundColor: 'white',
              backgroundGradientTo: 'white',
              backgroundGradientFrom: 'white',
              backgroundGradientFromOpacity: 0,
              backgroundGradientFromOpacity: 0,
              decimalPlaces: 0,
              color: (opacity = 1) => 'silver',
              labelColor: (opacity = 1) => '#29B4DC',
            }}
            style={{ position: 'absolute', top: 20 }}
          />
          <View>
            <FlatList
              horizontal
              data={this.state.statisticPeriod}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={
                    item.active ? styles.activePeriod : { marginHorizontal: 8 }
                  }
                  onPress={() => this.handlePressFilter(item.id)}>
                  <Text style={styles.textPeriod}>{item.title}</Text>
                </TouchableOpacity>
              )}
              style={{
                maxHeight: 20,
              }}
            />
          </View>
        </View>
        <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
          <View style={styles.container}>
            <Text style={styles.mainText}>
              Edit Safe Threshold
            </Text>
            <View style={styles.percentContainer}>
              <Text style={styles.spo2}>SPO2 </Text>
              <Text style={styles.percentNumber}>{Math.trunc(this.state.percent)}</Text>
              <Text style={styles.percent}>  percent</Text>
            </View>
          </View>
          <Slider
            minimumValue={88}
            maximumValue={95}
            step={0.05}
            minimumTrackTintColor='#C1C1C1'
            maximumTrackTintColor='#C1C1C1'
            thumbTintColor='#4DC2BF'
            value={this.state.percent}
            trackStyle={{
              height: 14,
              marginHorizontal: 25,
              borderRadius: 40
            }}
            thumbStyle={{ height: 38, width: 38, borderRadius: 50 }}
            onValueChange={value => this.setState({ percent: value })}
          />
          <View style={styles.sliderPercent}>
            <Text style={styles.smallPercents}>88%</Text>
            <Text style={styles.smallPercents}>95%</Text>
          </View>
          <MButton
            title="Call 911"
            onPress={this.onPressBack}
            buttonStyle={{ backgroundColor: '#F73B3B', marginHorizontal: '35%', marginVertical: 30 }}
          />
        </View>
      </ScrollView>
    );
  }
}

export default Dashboard;
