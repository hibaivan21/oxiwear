import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Switch from 'react-native-switch-pro';

import styles from './styles';
import colors from '../../../constants/colors'
import { DoneButton } from '../../../components/Buttons'


class LocationServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: true
        };
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View style={{ flex: 1 , backgroundColor: '#FFFFFF'}}>
                <View style={styles.container}>
                    <Text style={styles.itemName}>
                        Location Services On/Off
                    </Text>
                    <Switch
                        value={this.state.active}
                        onSyncPress={() => { }}
                        width={55}
                        height={15}
                        circleColorActive={colors.BUTTON_BACKGROUND}
                        backgroundInactive="transparent"
                        style={
                            this.state.active === false
                                ? styles.switcherContainer
                                : styles.switcherContainerActive
                        }
                        circleStyle={styles.circleContainer}
                        backgroundActive="transparent"
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 20 }}>
                    <DoneButton
                        title="Done"
                        onPress={this.onPressBack}
                    />

                </View>
            </View>
        );
    }
}

export default LocationServices;
