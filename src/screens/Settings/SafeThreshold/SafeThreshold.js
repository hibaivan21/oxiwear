import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Slider from "react-native-slider";

import styles from './styles';
import colors from '../../../constants/colors'
import { DoneButton } from '../../../components/Buttons'


class SafeThreshold extends Component {
    constructor(props) {
        super(props);
        this.state = {
            percent: 88
        };
    }

    onPressBack = () => {
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <View style={styles.container}>
                    <Text style={styles.mainText}>
                        Set Safe Threshold
                    </Text>

                    <View style={styles.percentContainer}>
                        <Text style={styles.spo2}>SPO2 </Text>
                        <Text style={styles.percentNumber}>{Math.trunc(this.state.percent)}</Text>
                        <Text style={styles.percent}>  percent</Text>
                    </View>
                </View>
                <Slider
                    minimumValue={88}
                    maximumValue={95}
                    step={0.05}
                    minimumTrackTintColor='#C1C1C1'
                    maximumTrackTintColor='#C1C1C1'
                    thumbTintColor='#4DC2BF'
                    value={this.state.percent}
                    trackStyle={{ 
                        height: 14, 
                        marginHorizontal: 25,
                        borderRadius: 40
                    }}
                    thumbStyle={{ height: 38, width: 38, borderRadius: 50}}
                    onValueChange={value => this.setState({ percent: value })}
                />
                <View style={styles.sliderPercent}>
                    <Text style={styles.smallPercents}>88%</Text>
                    <Text style={styles.smallPercents}>95%</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 20 }}>
                    <DoneButton
                        title="Done"
                        onPress={this.onPressBack}
                    />

                </View>
            </View>
        );
    }
}

export default SafeThreshold;
