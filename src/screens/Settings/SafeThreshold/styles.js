import {StyleSheet} from 'react-native';
import {colors} from '../../../constants';

export default StyleSheet.create({
  container: {
      flex: 1,
      paddingTop: '12%',
      paddingLeft: 20
  },
  mainText: {
      fontSize: 18,
      fontWeight: '700',
      color: '#505050'
  },
  percentContainer: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center',
    paddingTop: 24
  },
  spo2: { 
    fontSize: 39,
    fontWeight: '700',
    color: colors.DONE_BUTTON_BACKGROUND
  },
  percentNumber: {
    fontSize: 39,
    fontWeight: '700',
    color: colors.HEADER_COLOR
  },
  percent: {
    fontSize: 19,
    color: colors.DONE_BUTTON_BACKGROUND,
    paddingTop: 20
  },
  sliderPercent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 25
  },
  smallPercents: {
    fontSize: 14

  }
});
