import {StyleSheet} from 'react-native';
import {fonts} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 24,
  },
  itemContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    height: 50,
    marginVertical: 5,
    alignItems: 'center',
    paddingHorizontal: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  itemTitle: {
    fontFamily: fonts.robotoRegular,
    fontSize: 16,
  },
});
