import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';

import {icons} from '../../../constants';
import styles from './styles';

const ItemMenu = ({item}) => (
  <TouchableOpacity style={styles.itemContainer} onPress={item.onPress}>
    <View style={{width: 40, marginRight: 20}}>
      <item.icon />
    </View>
    <Text style={styles.itemTitle}>{item.title}</Text>
  </TouchableOpacity>
);

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {
          id: 0,
          title: 'Alerts and Notifications',
          icon: icons.Alert,
          onPress: () => {
            this.props.navigation.navigate('AlertsAndNotifications');
          },
        },
        {
          id: 1,
          title: 'Location Services',
          icon: icons.Location,
          onPress: () => {
            this.props.navigation.navigate('LocationServices');
          },
        },
        {
          id: 2,
          title: 'Unit Preferences',
          icon: icons.Pencil,
          onPress: () => {this.props.navigation.navigate('UnitPreferences')},
        },
        {
          id: 5,
          title: 'Personalization',
          icon: icons.Personalization,
          onPress: () => {
            this.props.navigation.navigate('Personalization');
          },
        },
        {
          id: 6,
          title: 'Safe Threshold',
          icon: icons.User,
          onPress: () => {
            this.props.navigation.navigate('SafeThreshold');
          },
        }
      ],
    };
  }
  render() {
    const {menu} = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          data={menu}
          renderItem={({item}) => <ItemMenu item={item} />}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}

export default Settings;
