import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    borderBottomWidth: 1,
    borderColor: colors.BORDER,
    alignItems: 'center',
    paddingVertical: 15,
  },
  itemName: {
    fontSize: 16,
    fontFamily: fonts.robotoRegular,
    color: colors.REGULAR_COLOR,
  },
  switcherContainer: {
    borderWidth: 0.5,
    padding: 5,
  },
  switcherContainerActive: {
    borderWidth: 0.5,
    padding: 5,
    borderColor: colors.MAIN_GRADIENT_START,
  },
  circleContainer: {
    borderWidth: 0.5,
  },
});
