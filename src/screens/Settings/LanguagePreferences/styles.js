import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.robotoBold,
    textAlign: 'center',
    color: colors.REGULAR_COLOR,
  },
});
