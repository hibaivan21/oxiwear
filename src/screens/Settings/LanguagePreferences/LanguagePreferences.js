import React, { Component } from 'react';
import { View, Text } from 'react-native';

import styles from './styles';
import { DoneButton } from '../../../components/Buttons'


class LanguagePreferences extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    // const {navigation} = this.props;
    // const routes = navigation.state.routes;
    return (
      <View style={{ flex: 1 , backgroundColor: '#FFFFFF'}}>
        <View style={{paddingTop: '20%'}}> 
          <Text style={styles.text}>
            Coming Soon
          </Text>
        </View>
        <View style={{flex: 1, justifyContent: 'flex-end', marginBottom: 20}}>
        <DoneButton
          title="Done"
          onPress={this.onPressBack}
        />

        </View>
      </View>
    );
  }
}

export default LanguagePreferences;
