import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {DefaultButton} from '../../../components/Buttons';
import {
  PremiumScreenOne,
  PremiumScreenTwo,
  PremiumScreenThree,
} from '../../../constants/images';

import styles from './styles';

class Premium extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {navigation} = this.props
    return (
      <View style={styles.container}>
        <View style={styles.topBlock}>
          <PremiumScreenOne width={134} height={132} />
          <View style={{flex: 1}}>
            <Text style={styles.text}>
              Predictive analysis and warnings based on your data
            </Text>
          </View>
        </View>
        <View style={styles.midleBlock}>
          <View style={{flex: 1}}>
            <Text style={styles.text}>Save or share detailed reports</Text>
          </View>
          <PremiumScreenTwo width={127} height={127} />
        </View>
        <View style={styles.bottomBlock}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <PremiumScreenThree width={127} height={127} />
            <View style={{flex: 1}}>
              <Text style={styles.text}>
                View weekly and monthly SPO2 and HR data  on your dashboard
              </Text>
            </View>
          </View>
        </View>
        <DefaultButton 
          title="START" 
          onPress={() => navigation.goBack()}
        />
      </View>
    );
  }
}

export default Premium;
