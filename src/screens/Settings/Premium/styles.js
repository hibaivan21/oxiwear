import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.robotoBold,
    textAlign: 'center',
    color: colors.REGULAR_COLOR,
  },
  topBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    flex: 4,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: colors.BORDER,
  },
  midleBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    paddingHorizontal: 12,
    borderColor: colors.BORDER,
    paddingVertical: 18,
    flex: 3,
  },
  bottomBlock: {
    alignItems: 'center',
    paddingHorizontal: 12,
    flex: 5,
    justifyContent: 'space-around'
  },
});
