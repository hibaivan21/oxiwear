import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import Switch from 'react-native-switch-pro';

import { colors } from '../../../constants';
import styles from './styles';
import { DoneButton } from '../../../components/Buttons'

const ItemPersonalization = ({ item, onPress }) => (
  <View style={styles.itemPersonalizationContainer}>
    <Text style={styles.itemName}>{item.name}</Text>
    <Switch
      value={item.active}
      onSyncPress={() => onPress(item.id)}
      width={55}
      height={15}
      circleColorActive={colors.BUTTON_BACKGROUND}
      backgroundInactive="transparent"
      style={
        item.active === false
          ? styles.switcherContainer
          : styles.switcherContainerActive
      }
      circleStyle={styles.circleContainer}
      backgroundActive="transparent"
    />
  </View>
);

class UnitPreferences extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionsList: [
        {
          id: 0,
          name: 'Celsius Units On/Off',
          active: true,
        }
      ],
    };
  }

  onSwitch = id => {
    const { optionsList } = this.state;
    const newOL = optionsList.map(item =>
      item.id === id ? { ...item, active: !optionsList[id].active } : { ...item },
    );
    this.setState({ optionsList: newOL });
  };

  render() {
    const {navigation} = this.props
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.optionsList}
          keyExtractor={(item, index) => item.id.toString()}
          renderItem={({ item }) => (
            <ItemPersonalization
              item={item}
              onPress={id => {
                this.onSwitch(id);
              }}
            />
          )}
        />
        <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 20 }}>
          <DoneButton
            title="Done"
            onPress={() => navigation.goBack()}
          />

        </View>
      </View>
    );
  }
}

export default UnitPreferences;
