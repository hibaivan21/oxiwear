import {StyleSheet} from 'react-native';
import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 15
  },
  itemPersonalizationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    borderBottomWidth: 1,
    borderColor: colors.BORDER,
    alignItems: 'center',
    paddingBottom: 15,
  },
  itemName: {
    fontSize: 16,
    fontFamily: fonts.robotoRegular,
    color: colors.REGULAR_COLOR,
  },
  topText: {
    paddingHorizontal: 20,
    textAlign: 'center',
    fontFamily: fonts.robotoBold,
    fontSize: 15,
    color: colors.REGULAR_COLOR,
  },
  topTextView: {
    borderBottomWidth: 1,
    borderColor: colors.BORDER,
    paddingVertical: 15,
  },
  switcherContainer: {
    borderWidth: 0.5,
    padding: 5,
  },
  switcherContainerActive: {
    borderWidth: 0.5,
    padding: 5,
    borderColor: colors.MAIN_GRADIENT_START,
  },
  circleContainer: {
    borderWidth: 0.5,
  },
});
