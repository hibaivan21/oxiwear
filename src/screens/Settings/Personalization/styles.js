import {StyleSheet} from 'react-native';

import {fonts, colors} from '../../../constants';

export default StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
  },
  titleStyle: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: colors.REGULAR_COLOR,
  },
  valueStyle: {
    fontFamily: fonts.robotoMedium,
    fontSize: 16,
    color: colors.REGULAR_COLOR,
  },
  profileItemContainer: {
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: 'silver',
    alignItems: 'center',
  },
  profileItemInputContainer: {
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: 'silver',
  },
});
