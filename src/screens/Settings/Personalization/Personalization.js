import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import {Input} from 'react-native-elements';

import {fonts} from '../../../constants';
import styles from './styles';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';

const ProfileItem = ({title, value}) => (
  <View style={styles.profileItemContainer}>
    <Text style={styles.titleStyle}>{title}</Text>
    <Text style={styles.valueStyle}>{value}</Text>
  </View>
);

const ProfileWithManyItem = ({title, value, onPress}) => (
  <View>
    <View style={styles.profileItemContainer}>
      <Text style={styles.titleStyle}>{title}</Text>
      <Text style={styles.valueStyle} onPress={() => onPress()}>
        + add
      </Text>
    </View>
    <FlatList data={value} renderItem={({item}) => <Text>{item}</Text>} />
  </View>
);


const ItemPersonalization = ({item, onPress}) => (
  <View style={styles.itemPersonalizationContainer}>
    <Text style={styles.itemName}>{item.name}</Text>
    <Switch
      value={item.active}
      onSyncPress={() => onPress(item.id)}
      width={55}
      height={15}
      circleColorActive={colors.BUTTON_BACKGROUND}
      backgroundInactive="transparent"
      style={
        item.active === false
          ? styles.switcherContainer
          : styles.switcherContainerActive
      }
      circleStyle={styles.circleContainer}
      backgroundActive="transparent"
    />
  </View>
);

class Personalization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {
        age: 32,
        gender: 'Female',
        weight: '130',
        phClass: 3,
        safeThreshold: '87-100',
        medication: [],
        reactions: [],
        emergencyContact: [],
        doctorNote: '',
      },
    };
  }

  handleAddNotes = value => {
    this.setState({
      profile: {
        ...this.state.profile,
      },
    });
  };

  render() {
    const {profile} = this.state;
    return (
      <KeyboardAvoidingScrollView contentContainerStyle={styles.container}>
        <ProfileItem title="Age" value={profile.age} />
        <ProfileItem title="Gender" value={profile.gender} />
        <ProfileItem title="Weight" value={profile.weight} />
        <ProfileItem title="Safe Threshold" value={profile.safeThreshold} />
        <ProfileWithManyItem
          title="Medication"
          value={profile.medication}
          onPress={() => {}}
        />
        <ProfileWithManyItem
          title="Allergies/Reactions"
          value={profile.reactions}
          onPress={() => {}}
        />
        <ProfileWithManyItem
          title="Emergency Contacts"
          value={profile.emergencyContact}
          onPress={() => {}}
        />
      </KeyboardAvoidingScrollView>
    );
  }
}

export default Personalization;
