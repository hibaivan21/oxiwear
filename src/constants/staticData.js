import React from 'react';
import {Dimensions} from 'react-native';
import {Logo, Logo_Warning, Speak911, Contact, Wear} from '../constants/images';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const authData = [
  {
    headerText: null,
    image: <Wear width={width * 0.4} />,
    footerText:
      'Continuous SPO2 Monitoring\nSPO2 - amount of oxygen in the blood',
  },
  {
    headerText: 'Personalize to your safe SPO2 threshold',
    image: <Logo_Warning width={width * 0.4} />,
    footerText: 'Discreet warning when SPO2 level is low ',
  },
  {
    headerText: 'Dial 911 with the press of a button',
    image: <Speak911 width={width * 0.4} />,
    footerText: 'Speak with 911 through your device',
  },
  {
    headerText: null,
    image: <Contact width={width * 0.4} />,
    footerText: 'Enter emergency contacts to enable text alerts',
  },
];

export {authData};
