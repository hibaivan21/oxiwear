import Wear from '../assets/images/wear.svg';
import Logo from '../assets/images/logo.svg';
import Logo_Warning from '../assets/images/logo-warning.svg';
import Speak911 from '../assets/images/speak911.svg';
import Contact from '../assets/images/contact.svg';
import Male from '../assets/icons/male.svg';
import Female from '../assets/icons/female.svg';
import PremiumScreenOne from '../assets/images/premium-screen-one.svg';
import PremiumScreenTwo from '../assets/images/premium-screen-two.svg';
import PremiumScreenThree from '../assets/images/premium-screen-three.svg';

const images = {
  LeftBLuetooth: require('../assets/images/left-bluetooth.png'),
  RightBLuetooth: require('../assets/images/right-bluetooth.png'),
};

export {
  images,
  Logo,
  Logo_Warning,
  Speak911,
  Contact,
  Wear,
  Male,
  Female,
  PremiumScreenOne,
  PremiumScreenTwo,
  PremiumScreenThree,
};
