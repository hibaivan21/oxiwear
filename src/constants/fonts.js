const fonts = {
  robotoBold: 'Roboto-Bold',
  robotoRegular: 'Roboto-Regular',
  robotoMedium: 'Roboto-Medium',
};

export default fonts;
