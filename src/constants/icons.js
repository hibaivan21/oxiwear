import DashboardNav from '../assets/icons/dashboard-nav.svg';
import JournalNav from '../assets/icons/journal-nav.svg';
import MedicalNav from '../assets/icons/medical-nav.svg';
import ProgressNav from '../assets/icons/progress-nav.svg';
import ReportsNav from '../assets/icons/reports-nav.svg';

//Settings Icon
import Alert from '../assets/icons/alert-settings.svg';
import Language from '../assets/icons/language-settings.svg';
import Location from '../assets/icons/location-settings.svg';
import Pencil from '../assets/icons/pencil-settings.svg';
import Personalization from '../assets/icons/personalization-settings.svg';
import Star from '../assets/icons/star-settings.svg';
import Sync from '../assets/icons/sync-settings.svg';
import User from '../assets/icons/user-settings.svg';
//
import PDF from '../assets/icons/pdf-icon.svg';
import Excel from '../assets/icons/excel-icon.svg';
//Journal
import Pills from '../assets/icons/pills.svg';
import Symptoms from '../assets/icons/symtoms.svg';
import Notes from '../assets/icons/notes.svg';

export default {
  DashboardNav,
  JournalNav,
  MedicalNav,
  ProgressNav,
  ReportsNav,
  //Settings
  Alert,
  Language,
  Location,
  Pencil,
  Personalization,
  Star,
  Sync,
  User,
  PDF,
  Excel,
  Pills,
  Symptoms,
  Notes,
};
