import React from 'react';
import {Provider} from 'react-redux';

import AppNavigation from './navigation';

import ErrorLogger from './components/ErrorLogger';

import store from './redux';

const App = () => (
  <Provider store={store}>
    <ErrorLogger>
      <AppNavigation />
    </ErrorLogger>
  </Provider>
);

export default App;
