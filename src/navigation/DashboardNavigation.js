import {createStackNavigator} from 'react-navigation-stack';

import {HomeHeader, DefaultHeader} from '../components/Headers';

import Dashboard from '../screens/Dashboard/Dashboard';
import SettingsNav from './SettingsNavigation';
import PowerLevel from '../screens/PowerLevel';
import {Platform} from 'react-native';

const DashboardNavigation = createStackNavigator(
  {
    Dashboard: {
      screen: Dashboard,
      navigationOptions: {
        title: '',
        header: HomeHeader,
        headerStyle: {
          height: Platform.OS === 'ios' ? 120 : 80,
        },
      },
    },
    Settings: {
      screen: SettingsNav,
      navigationOptions: {
        headerShown: false,
      },
    },
    PowerLevel: {
      screen: PowerLevel,
      navigationOptions: {
        title: 'Developer Test Module',
        header: DefaultHeader
      },
    },
  },
  {
    headerMode: 'screen',
  },
);

export default DashboardNavigation;
