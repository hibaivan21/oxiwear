import {createStackNavigator} from 'react-navigation-stack';

import SignInScreen from '../screens/Auth/SignIn';
import SignUpScreen from '../screens/Auth/SignUp';
import AuthScreen from '../screens/Auth';
import Connection from '../screens/Connection/connection/'
import DeviceConnection from '../screens/Connection/DeviceConnection';
import ConnectionProcess from '../screens/Connection/ConnectionProcess';
import DeviceConnected from '../screens/Connection/DeviceConnected';
import CreateProfile from '../screens/CreateProfile';

const AuthNavigation = createStackNavigator(
  {
    Auth: {
      screen: AuthScreen,
    },
    SignIn: {
      screen: SignInScreen,
    },
    SignUp: {
      screen: SignUpScreen,
    },
    DeviceConnection: {
      screen: DeviceConnection,
    },
    Connection: {
      screen: Connection,
    },
    ConnectionProcess: {
      screen: ConnectionProcess,
    },
    DeviceConnected: {
      screen: DeviceConnected,
    },
    CreateProfile: {
      screen: CreateProfile,
    },
  },

  {
    defaultNavigationOptions: {
      headerShown: false,
    },
    initialRouteName: 'Auth',
  },
);

export default AuthNavigation;
