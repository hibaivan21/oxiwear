import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

import {DefaultTabHeader, DefaultHeader} from '../components/Headers';

import Journal from '../screens/Journal/Journal/Journal';
import Medication from '../screens/Journal/Medications';
import Symptoms from '../screens/Journal/Symptoms';

const SemiJournalNavigation = createMaterialTopTabNavigator(
  {
    Medication: {
      screen: Medication,
      navigationOptions: {
        title: 'Medication',
      },
    },
    Symptoms: {
      screen: Symptoms,
      navigationOptions: {
        title: 'Symptoms',
      },
    },
  },
  
  {
    tabBarComponent: DefaultTabHeader,
  },
);

const JournalNavigation = createStackNavigator(
  {
    Journal: {
      screen: Journal,
      navigationOptions: {
        title: 'Journal',
        header: DefaultHeader,
      },
    },
    SemiJournal: {
      screen: SemiJournalNavigation,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    headerMode: 'screen',
  },
);

export default JournalNavigation;
