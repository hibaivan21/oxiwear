import {createStackNavigator} from 'react-navigation-stack';

import {DefaultHeader} from '../components/Headers';

import MedicalID from '../screens/MedicalID/MedicalID';

const MedicalNavigation = createStackNavigator(
  {
    MedicalID: {
      screen: MedicalID,
      navigationOptions: {
        title: 'Medical ID',
        header: DefaultHeader,
      },
    },
  },
  {
    headerMode: 'screen',
  },
);

export default MedicalNavigation;
