import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';

import Progress from '../screens/Progress';

import DefaultHeader from '../components/Headers/DefaultHeader';

const ProgressNavigation = createStackNavigator(
  {
    Progress: {
      screen: Progress,
      navigationOptions: ({navigation}) => {
        return {
          title: 'Progress',
          header: props => <DefaultHeader {...props} showTest={true} />,
        };
      },
    },
  },
  {
    headerMode: 'screen',
  },
);

export default ProgressNavigation;
