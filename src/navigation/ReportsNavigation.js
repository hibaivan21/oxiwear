import {createStackNavigator} from 'react-navigation-stack';

import {DefaultHeader} from '../components/Headers';

import Reports from '../screens/Reports/Report';

const DashboardNavigation = createStackNavigator(
  {
    Reports: {
      screen: Reports,
      navigationOptions: {
        title: 'Reports',
        header: DefaultHeader,
      },
    },
  },
  {
    headerMode: 'screen',
  },
);

export default DashboardNavigation;
