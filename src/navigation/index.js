import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import AuthNavigation from './AuthNavigation';
import AppNavigation from './AppNavigation';
import PreLoadingScreen from '../screens/PreLoadingScreen'
const MainNavigation = createSwitchNavigator(
  {
    Splash: PreLoadingScreen,
    Auth: AuthNavigation,
    App: AppNavigation,
  },
  {
    initialRouteName: 'Splash',
  },
);

export default createAppContainer(MainNavigation);
