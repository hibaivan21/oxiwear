import {createBottomTabNavigator} from 'react-navigation-tabs';
import {icons} from '../constants';

import DashboardNavigation from './DashboardNavigation';
import ReportsNavigation from './ReportsNavigation';

const AppNavigation = createBottomTabNavigator(
  {
    Dashboard: {
      screen: DashboardNavigation,
      navigationOptions: {
        tabBarIcon: icons.DashboardNav,
      },
    },
    Reports: {
      screen: ReportsNavigation,
      navigationOptions: {
        tabBarIcon: icons.ReportsNav,
      },
    }
  },
  {
    tabBarOptions: {
      showLabel: false,
      style: {
        height: 100,
      },
    },
  },
);

export default AppNavigation;
