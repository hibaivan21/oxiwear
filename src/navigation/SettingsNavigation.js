import {createStackNavigator} from 'react-navigation-stack';

import {DefaultHeader} from '../components/Headers';
import {SettingsHeader} from '../components/Headers'

import Settings from '../screens/Settings/Settings';
import Premium from '../screens/Settings/Premium';
import Personalization from '../screens/Settings/Personalization';
import LanguagePreferences from '../screens/Settings/LanguagePreferences'
import AlertsAndNotifications from '../screens/Settings/AlertsAndNotifications'
import LocationServices from '../screens/Settings/LocationServices'
import SafeThreshold from '../screens/Settings/SafeThreshold'
import UnitPreferences from '../screens/Settings/UnitPreferences'

const SettingsNavigation = createStackNavigator({
    Settings: {
      screen: Settings,
      navigationOptions: {
        header: DefaultHeader,
        headerStyle: {
          height: 166,
        },
      },
    },
    Premium: {
      screen: Premium,
      navigationOptions: {
        title: 'Premium Account',
        header: DefaultHeader,
        headerStyle: {
          height: 166,
        },
      },
    },
    Personalization: {
      screen: Personalization,
      navigationOptions: {
        title: 'Personal Information',
        header: SettingsHeader,
        headerStyle: {
          height: 166,
        },
      },
    },
    UnitPreferences: {
      screen: UnitPreferences,
      navigationOptions: {
        title: 'Temperature Preference',
        header: SettingsHeader,
        headerStyle: {
          height: 166,
        },
      },
    },

    LanguagePreferences: {
      screen: LanguagePreferences,
      navigationOptions: {
        title: 'Language Preference',
        header: SettingsHeader        
      },
    },
    AlertsAndNotifications: {
      screen: AlertsAndNotifications,
      navigationOptions: {
        title: 'Alerts And Notifications',
        header: SettingsHeader        
      },
    },
    LocationServices: {
      screen: LocationServices,
      navigationOptions: {
        title: 'Location Services',
        header: SettingsHeader        
      },
    },
    SafeThreshold: {
      screen: SafeThreshold,
      navigationOptions: {
        title: 'Safe Threshold',
        header: SettingsHeader        
      },
    },
  },
);

export default SettingsNavigation;
