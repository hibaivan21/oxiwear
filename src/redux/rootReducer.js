import {combineReducers} from 'redux';

import authReducer from './auth/reducer';
import errorReducer from './error/reducer';
import profileReducer from './profile/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  errorLogger: errorReducer,
  profile: profileReducer,
});

export default rootReducer;
