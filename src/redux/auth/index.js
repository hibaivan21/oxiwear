import reducer from './reducer';

import * as authAction from './actions';
import * as authTypes from './types';

export {authAction, authTypes};

export default reducer;
