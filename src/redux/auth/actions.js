import * as types from './types';

const setUser = user => ({
  type: types.SET_USER,
  payload: user,
});

const setLoading = loading => ({
  type: types.SET_LOADING,
  payload: loading,
});

const setSuccess = success => ({
  type: types.SET_SUCCESS,
  payload: success,
});

export const logout = () => ({
  type: types.SET_LOGOUT,
});

export const signUp = data => dispatch => {};

export const signIn = data => dispatch => {};
