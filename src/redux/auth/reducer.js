import * as types from './types';

const initState = {
  user: null,
  loading: false,
  success: false,
};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SET_USER: {
      return {
        ...state,
        user: action.payload,
      };
    }
    case types.SET_LOADING: {
      return {
        ...state,
        loading: action.payload,
      };
    }
    case types.SET_SUCCESS: {
      return {
        ...state,
        success: action.payload,
      };
    }
    case types.SET_LOGOUT: {
      return {
        ...state,
        user: null,
        loading: false,
        success: false,
      };
    }
    default:
      return state;
  }
};

export default authReducer;
