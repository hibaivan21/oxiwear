import * as types from './types';

const initState = {
  createProfile: {
    sex: null,
    yearBorn: null,
    monthBorn: null,
    weight: null,
    cardionvascular: false,
    ph: false,
    phClass: 1,
    spo2: null,
  },
};

const profileReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SET_PROFILE_FIELD: {
      return {
        ...state,
        createProfile: {
          ...state.createProfile,
          [action.field]: action.value,
        },
      };
    }

    default:
      return state;
  }
};

export default profileReducer;
