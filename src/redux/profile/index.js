import reducer from './reducer';

import * as profileAction from './actions';
import * as profileTypes from './types';

export {profileAction, profileTypes};

export default reducer;
