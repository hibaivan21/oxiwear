import * as types from './types';

export const setProfileField = (field, value) => ({
  type: types.SET_PROFILE_FIELD,
  field,
  value,
});
