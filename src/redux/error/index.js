import reducer from './reducer';

import * as errorAction from './actions';
import * as errorTypes from './types';

export {errorAction, errorTypes};

export default reducer;
