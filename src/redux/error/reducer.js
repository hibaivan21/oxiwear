import * as types from './types';

const initState = {
  typeError: 'error',
  error: null,
};

const errorReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SET_ERROR: {
      return {
        ...state,
        typeError: action.typeError,
        error: action.error,
      };
    }
    default:
      return state;
  }
};

export default errorReducer;
