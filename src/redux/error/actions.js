import * as types from './types';

export const setError = (typeError, error) => ({
  type: types.SET_ERROR,
  typeError,
  error,
});
